var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
documentos: {
        nombre: {type: String, default: ""},
        info: {type: String, default: "web"},
        doc: {type: mongoose.Schema.Types.Mixed},
        status: {type: Number, default: 1}
    }
 */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("documentos", req.query, [],["_id","nombre","info","doc","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("documentos", {"_id": req.params.id}, [],["_id","nombre","info","doc","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("documentos", req.body, ["_id","nombre","info","doc","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("documentos", {"_id": req.params.id}, req.body, ["_id","nombre","info","doc","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("documentos", {"_id": req.params.id}, {"status": 0}, ["_id","nombre","info","doc","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
