var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
tiposUbicaciones: {
        nombre: {type: String, default: ""},
        imagen: {type: String, default: ""},
        variableEnvio: {type: String, default: ""},
        status: {type: Number, default: 1}
    }
["_id","nombre","imagen","status"]
      */
module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("serviciosAdicionalesSolicitudes", req.query, [{path:'servicioAdicional', model:'serviciosAdicionales', select:"_id nombre imagen status"}],["_id","servicioAdicional","latitud","longitud","usuario","fechaCreacion","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("serviciosAdicionalesSolicitudes", {"_id": req.params.id}, [{path:'servicioAdicional', model:'serviciosAdicionales', select:"_id nombre imagen status"}],["_id","servicioAdicional","latitud","longitud","usuario","fechaCreacion","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("serviciosAdicionalesSolicitudes", req.body, ["_id","servicioAdicional","latitud","longitud","usuario","fechaCreacion","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("serviciosAdicionalesSolicitudes", {"_id": req.params.id}, req.body, ["_id","servicioAdicional","latitud","longitud","usuario","fechaCreacion","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("serviciosAdicionalesSolicitudes", {"_id": req.params.id}, {"status": 0}, ["_id","servicioAdicional","latitud","longitud","usuario","fechaCreacion","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminarFisico: function (req, res) {
        try {
            mongoBuilder.eliminar("serviciosAdicionalesSolicitudes", {"_id": req.params.id}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    limpiar: function (req, res) {
        try {
            mongoBuilder.eliminar("serviciosAdicionalesSolicitudes", {}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
