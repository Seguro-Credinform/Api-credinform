var Sequelize = require('sequelize');
var sequelize = require('../configs/database');
var modelos = require('../services/modelos');

module.exports = {
    emergencias: sequelize.define('emergencias', modelos.emergencias, {timestamps: false}),
    informaciones: sequelize.define('informaciones', modelos.informaciones, {timestamps: false}),
    ofertas: sequelize.define('ofertas', modelos.ofertas, {timestamps: false}),
    oficinas: sequelize.define('oficinas', modelos.oficinas, {timestamps: false}),
    llaves: sequelize.define('llaves', modelos.llaves, {timestamps: false}),
    sesiones: sequelize.define('sesiones', modelos.sesiones, {timestamps: false}),
    tiposMensajes: sequelize.define('tiposMensajes', modelos.tiposMensajes, {timestamps: false}),
    mensajes: sequelize.define('mensajes', modelos.mensajes, {timestamps: false}),
    mensajesRespuestas: sequelize.define('mensajesRespuestas', modelos.mensajesRespuestas, {timestamps: false}),
    polizas: sequelize.define('polizas', modelos.polizas, {timestamps: false}),
    hospitales: sequelize.define('hospitales', modelos.hospitales, {timestamps: false})
};

