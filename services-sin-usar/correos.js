var nodemailer = require('nodemailer');
var strings = require('../configs/strings');

// create reusable transporter object using the default SMTP transport
module.exports = {
    transporter: nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'contacto.tecomca@gmail.com',
            pass: '1234567890t'
        }
    }),
    origen: '"Tecomca" <contacto.tecomca@gmail.com>',
    headerTemplate: `<div style="width: 100%;">
    <div style="background: #ffffff; width: 100%; text-align: center">
        <div style="padding: 30px;">
            <img src="`+ strings.imagenes.url + strings.imagenes.iconos +`logo.png" height="100px" width="auto">
        </div>  
        <div style="background: #fa0200; width: 100%; height: 40px;">
        </div>   
    </div>

</div>`,
    footerTemplate: `<div style="width: 100%;">
    <div style="background: #323232; width: 100%; text-align: center; font-size: 15px;">
        <div style="padding: 10px;">
            <a href="bonmeats.com" style="color: #ffffff; text-decoration:none;">Bonmeats.com</a><br>
            <br>
            
        </div>  
    </div>  
    <div style="background: #2e2e2e; width: 100%; color: #ffffff; text-align: center;">
        <div style="padding: 10px; font-size: 11px;">Bonmeats.com - all right reserved copyright</div>
    </div>   
</div>`,
    bodyHeaderTemplate: `
<div style="width: 100%;">
<div style="padding: 80px;">
`,
    bodyFooterTemplate: `
</div>
</div>
`,
    enviarCorreoEstandar: function (nombre, destino, titulo, mensaje, callback) {
        this.transporter.sendMail({
            from: this.origen, // sender address
            to: nombre + ', ' + destino, // list of receivers
            subject: titulo, // Subject line
            html: this.headerTemplate + this.bodyHeaderTemplate + mensaje + this.bodyFooterTemplate + this.footerTemplate
        }, function (error, info) {
            if (error) {
                return callback(0, 1);//no se envio el mensaje hay un error
            } else {
                return callback(1, 0);//se envio el mensaje hay un error
            }

        });


    }
}