module.exports = {
//este primer schema es obligatorio, los demas objtos y arrays son por las particularidades de las consultas

    "llaves": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idLlave": {"nombreReal": "id_llave", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "llave": {"nombreReal": "llave", "type": "string", "query": "like", "actualizar": "si"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"}
            }
        }
    },
    "sesiones": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "token": {"nombreReal": "token", "type": "string", "query": "like", "actualizar": "si"},
                "informacion": {"nombreReal": "informacion", "type": "string", "query": "like", "actualizar": "si"},
                "usuario": {"nombreReal": "usuario", "type": "string", "query": "like", "actualizar": "si"},
                "creacion": {"nombreReal": "creacion", "type": "string", "query": "like", "actualizar": "si"}
            }
        }
    },
    "emergencias": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idEmergencia": {"nombreReal": "id_emergencia", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "nombre": {"nombreReal": "nombre", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "telefono": {"nombreReal": "telefono", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "correo": {"nombreReal": "correo", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                 "imagen": {"nombreReal": "imagen","type": "string", "dato": "imagen", "urlImagen": "emergencias", "urlImagenId": "idEmergencia",  "urlImagenDefectoPequeno": "https://dummyimage.com/100x100/faba1a/4d4d4d.png", "urlImagenDefectoMediano": "https://dummyimage.com/600x600/faba1a/4d4d4d.png", "urlImagenDefectoGrande": "https://dummyimage.com/800x800/faba1a/4d4d4d.png", "actualizar": "si", "primary": "no"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"}
            }
        }
    },
    "informaciones": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idInformacion": {"nombreReal": "id_informacion", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "descripcion": {"nombreReal": "descripcion","type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "titulo": {"nombreReal": "titulo", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "imagen": {"nombreReal": "imagen","type": "string", "dato": "imagen", "urlImagen": "informaciones", "urlImagenId": "idInformacion",  "urlImagenDefectoPequeno": "https://dummyimage.com/100x100/faba1a/4d4d4d.png", "urlImagenDefectoMediano": "https://dummyimage.com/600x600/faba1a/4d4d4d.png", "urlImagenDefectoGrande": "https://dummyimage.com/800x800/faba1a/4d4d4d.png", "actualizar": "si", "primary": "no"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"}
            }
        }
    },
    "ofertas": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idOferta": {"nombreReal": "id_oferta", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "titulo": {"nombreReal": "titulo","type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "descripcion": {"nombreReal": "descripcion", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "imagen": {"nombreReal": "imagen","type": "string", "dato": "imagen", "urlImagen": "ofertas", "urlImagenId": "idOferta",  "urlImagenDefectoPequeno": "https://dummyimage.com/100x100/faba1a/4d4d4d.png", "urlImagenDefectoMediano": "https://dummyimage.com/600x600/faba1a/4d4d4d.png", "urlImagenDefectoGrande": "https://dummyimage.com/800x800/faba1a/4d4d4d.png", "actualizar": "si", "primary": "no"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"}
            }
        }
    },
    "oficinas": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idOficina": {"nombreReal": "id_oficina", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "nombre": {"nombreReal": "nombre", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "telefono": {"nombreReal": "telefono", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "correo": {"nombreReal": "correo", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "direccion": {"nombreReal": "direccion", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0,  "actualizar": "si"}
            }
        }
    },
    "tiposMensajes": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idTipoMensaje": {"nombreReal": "id_tipo_mensaje", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "nombre": {"nombreReal": "nombre", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "imagen": {"nombreReal": "imagen","type": "string", "dato": "imagen", "urlImagen": "tiposMensajes", "urlImagenId": "idOferta",  "urlImagenDefectoPequeno": "https://dummyimage.com/100x100/faba1a/4d4d4d.png", "urlImagenDefectoMediano": "https://dummyimage.com/600x600/faba1a/4d4d4d.png", "urlImagenDefectoGrande": "https://dummyimage.com/800x800/faba1a/4d4d4d.png", "actualizar": "si", "primary": "no"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"}
            }
        }
    },
    "mensajes": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idMensaje": {"nombreReal": "id_mensaje", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "idTipoMensaje": {"nombreReal": "id_tipo_mensaje", "type": "integer", "minimum": 0, "actualizar": "si"},
                "asunto": {"nombreReal": "asunto", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "respuesta": {"nombreReal": "respuesta", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "mensaje": {"nombreReal": "mensaje", "type": "string", query: "like", "actualizar": "si", "primary": "no"}, 
                "username": {"nombreReal": "username", "type": "string", query: "like", "actualizar": "si", "primary": "no"},    
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"},
                "creacion": {"nombreReal": "creacion", "type": "string", "query": "like", "actualizar": "si"}
            }
        }
    },
    "mensajesRespuestas": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idMensajeRespuesta": {"nombreReal": "id_mensaje_respuesta", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "idMensaje": {"nombreReal": "id_mensaje", "type": "integer", "minimum": 0, "actualizar": "si"},
                "mensaje": {"nombreReal": "mensaje", "type": "string", query: "like", "actualizar": "si", "primary": "no"}, 
                "username": {"nombreReal": "username", "type": "string", query: "like", "actualizar": "si", "primary": "no"},    
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"},
                "creacion": {"nombreReal": "creacion", "type": "string", "query": "like", "actualizar": "si"}
            }
        }
    },
    "polizas": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idPoliza": {"nombreReal": "id_poliza", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "nombre": {"nombreReal": "nombre", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "descripcion": {"nombreReal": "descripcion", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "idTipoBien": {"nombreReal": "id_tipo_bien", "type": "integer", "minimum": 0, "actualizar": "si"},
                "idRiesgo": {"nombreReal": "id_riesgo", "type": "integer", "minimum": 0, "actualizar": "si"},
                "idPresupuesto": {"nombreReal": "id_presupuesto", "type": "integer", "minimum": 0, "actualizar": "si"},
                 "imagen": {"nombreReal": "imagen","type": "string", "dato": "imagen", "urlImagen": "polizas", "urlImagenId": "idEmergencia",  "urlImagenDefectoPequeno": "https://dummyimage.com/100x100/faba1a/4d4d4d.png", "urlImagenDefectoMediano": "https://dummyimage.com/600x600/faba1a/4d4d4d.png", "urlImagenDefectoGrande": "https://dummyimage.com/800x800/faba1a/4d4d4d.png", "actualizar": "si", "primary": "no"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"}
            }
        }
    },
    "hospitales": {
        "schema": {
            "additionalProperties": false,
            "properties": {
                "idHospital": {"nombreReal": "id_hospital", "type": "array", "minItems": 1, "items": {"type": "integer", "minimum": 1}, "query": "in", "actualizar": "no", "primary": "si"},
                "nombre": {"nombreReal": "nombre", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "telefono": {"nombreReal": "telefono", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "correo": {"nombreReal": "correo", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                "direccion": {"nombreReal": "direccion", "type": "string", query: "like", "actualizar": "si", "primary": "no"},
                 "imagen": {"nombreReal": "imagen","type": "string", "dato": "imagen", "urlImagen": "emergencias", "urlImagenId": "idEmergencia",  "urlImagenDefectoPequeno": "https://dummyimage.com/100x100/faba1a/4d4d4d.png", "urlImagenDefectoMediano": "https://dummyimage.com/600x600/faba1a/4d4d4d.png", "urlImagenDefectoGrande": "https://dummyimage.com/800x800/faba1a/4d4d4d.png", "actualizar": "si", "primary": "no"},
                "status": {"nombreReal": "status", "type": "integer", "minimum": 0, "actualizar": "si"}
            }
        }
    }
};
