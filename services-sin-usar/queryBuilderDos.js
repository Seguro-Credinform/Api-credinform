var utils = require('../services/utils');
var strings = require('../configs/strings');
var Ajv = require('ajv');
var ajv = new Ajv({useDefaults: true});
var _ = require('lodash');
var mysql = require('mysql2');
var conector = strings.mysql;

/*variables de configuracion*/
module.exports = {
    eliminarBaseDeDatos: function (nombre, callback) {
        var connecta = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password});
        connecta.query('DROP DATABASE IF EXISTS ' + nombre, function (err, results, fields) {
            return callback(results);
        });
    },
    crearBaseDeDatos: function (nombre, callback) {
        var connecta = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password});
        connecta.query('CREATE DATABASE ' + nombre, function (err, results, fields) {
            return callback(results); // results contains rows returned by server
        });
    }
    ,
    crearTabla: function (nombre, query, callback) {
        console.log(query);
        var connection = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password, database: conector.database});
        connection.query('DROP TABLE IF EXISTS ' + nombre, function (err, results, fields) {
            connection.query(query, function (erra, resultas, fieldsa) {
                return callback(resultas); // results contains rows returned by server
            });
        });
    }
    ,
    crearVista: function (nombre, query, callback) {
        var connection = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password, database: conector.database});
        connection.query('DROP VIEW IF EXISTS ' + nombre, function (err, results, fields) {
            connection.query(query, function (erra, resultas, fieldsa) {
                return callback(resultas); // results contains rows returned by server
            });
        });
    }
    ,
    insertarRegistro: function (tabla, campos, validadores, respuesta, callback) {
        var nombreCampos = "(";
        var contenidoCampos = "(";
        //se arma la consulta
        for (var indice in campos) {
            if (typeof (validadores.schema.properties[indice]) !== "undefined") {
                nombreCampos += validadores.schema.properties[indice].nombreReal + " ,";
                if (validadores.schema.properties[indice].type === "string") {
                    contenidoCampos += "'" + campos[indice] + "' ,";
                } else {
                    contenidoCampos += campos[indice] + " ,";
                }
            }

        }
        //se arma la respuesta
        var seleccionables = "";
        for (var indice in respuesta) {
            seleccionables += validadores.schema.properties[respuesta[indice]].nombreReal + ' as ' + respuesta[indice] + ' ,';
        }
        var primary = "";
        for (var indice in validadores.schema.properties) {
            if ((typeof (validadores.schema.properties[indice].primary) === "string") && (validadores.schema.properties[indice].primary === "si")) {
                primary = validadores.schema.properties[indice].nombreReal;
            }
        }
        var consulta = "INSERT INTO " + tabla + " " + nombreCampos.substr(0, nombreCampos.length - 1) + ") VALUES " + contenidoCampos.substr(0, contenidoCampos.length - 1) + ")";
        console.log(consulta);

        var connection = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password, database: conector.database});
        connection.query(consulta, function (err, results, fields) {
            console.log('SELECT ' + seleccionables.substr(0, seleccionables.length - 1) + ' FROM ' + tabla + ' ORDER BY ' + primary + ' DESC LIMIT 1');
            connection.query('SELECT ' + seleccionables.substr(0, seleccionables.length - 1) + ' FROM ' + tabla + ' ORDER BY ' + primary + ' DESC LIMIT 1', function (erra, resulta, field) {
                return callback(resulta); // results contains rows returned by server
            });
        });
    },
    ejecutarQuery: function (query, callback) {
        var connection = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password, database: conector.database});
        connection.query(query, function (erra, resultas, fieldsa) {
            return callback(resultas); // results contains rows returned by server
        });
    },
    actualizarRegistro: function (tabla, where, campos, validadores, callback) {
        var connection = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password, database: conector.database});
        var consulta = "";
        var condicion = "";
        if (typeof (campos) === "object") {
            for (var i in campos) {
                consulta = consulta + " , " + this.traducirCampo(validadores, i) + " = " + this.traducirCondicion(validadores, i, campos[i]);
            }
            consulta = consulta.substring(2, consulta.length)
        } else if (typeof (campos) === "string") {
            consulta = campos;
        } else {
            return ("error debe ser un objeto o un string el segundo parametro");
        }

        if (typeof (where) === "object") {
            for (var i in where) {
                condicion = condicion + " and " + this.traducirCampo(validadores, i) + " = " + this.traducirCondicion(validadores, i, where[i]);
            }
            condicion = condicion.substring(4, condicion.length)
        } else if (typeof (where) === "string") {
            condicion = where;
        } else {
            return ("error debe ser un objeto o un string el segundo parametro");
        }
        consulta = "UPDATE " + tabla + " SET " + consulta + " WHERE " + condicion + ";";
        console.log(consulta);
        connection.query(consulta, function (err, results, fields) {
            return callback(results); // results contains rows returned by server
        });
    },
    eliminarRegistro: function (tabla, campos, validadores, callback) {
        var connection = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password, database: conector.database});
        var consulta = "";
        if (typeof (campos) === "object") {
            for (var i in campos) {
                consulta = consulta + " and " + this.traducirCampo(validadores, i) + " = " + this.traducirCondicion(validadores, i, campos[i]);
            }
            consulta = consulta.substring(4, consulta.length)
        } else if (typeof (campos) === "string") {
            consulta = campos;
        } else {
            return ("error debe ser un objeto o un string el segundo parametro");
        }
        consulta = "DELETE FROM " + tabla + " WHERE" + consulta + ";";
        console.log(consulta);
        connection.query(consulta, function (err, results, fields) {
            return callback(results); // results contains rows returned by server
        });
    },
    obtenerRegistro: function (tabla, campos, validadores, respuesta, callback) {
        var consulta = "select "+tabla;
        this.paginacio(page, perPage, function(respuesta, error){
          consulta = "select ";
        return callback(consulta); 
        });
        
    },
    paginacio: function (page, perPage, callback) {
        if (typeof(page)==="undefined"){
            page = 1;
        }
        if (typeof(perPage)==="undefined"){
            page = 20;
        }
        if (parseInt(perPage)>100){
           return callback ("","el perPage debe ser menor que 100");
        }
        if (parseInt(page)===0){
           return callback ("","la pagina debe ser mayor a 0");
        }
        if (parseInt(perPage)===0){
           return callback ("","el perPage debe ser mayor a 0");
        }
        return callback (" limit "+parseInt(perPage)+" offset "+(parseInt(perPage)*(parseInt(page)-1))+" ");
    },
    ordenar: function (validadores, order, callback) {
        var respuesta = "";
        var propiedades = order.split(',')
        for (var i in propiedades){
            var mini = info[propiedades].split(':');
            if (mini.length===2){
                respuesta = respuesta+ " , "+ mini[0]+ " "+ mini[1];
            }else{
               respuesta = respuesta+ " , "+ mini[0]+ " "; 
            }
        }
        if (respuesta===""){
            return callback ("", "");
        }else{
           return callback (" ORDER BY +"+respuesta, ""); 
        }
        
    },
    traducirCampo(validadores, campo) {
        return validadores.schema.properties[campo].nombreReal;
    },
    traducirCondicion(validadores, campo, valor) {
        if (validadores.schema.properties[campo].type === "string") {
            return '"' + valor + '"';
        } else {
            return valor;
        }
    }


}
/*
 queryBuilder.page(req.query.page, req.query.perPage, res),
 queryBuilder.contarInmuebles(req.query.count, res),
 queryBuilder.order(['idMedida', 'nombre','count'], req.query.sort, res),
 queryBuilder.addProperty(['idMedida', 'nombre'], req.query.fields, res),
 queryBuilder.condicionales(req.query, validadores.medidas.schema, {}, ["sort", "page", "perPage", "fields", "count"], [], res),
 */