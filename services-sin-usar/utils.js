var strings = require('../configs/strings');
var crypto = require('crypto');
const secret = '5cuguvZ4Az';
var utils = {
//verifica si el array a esta en el array b
    intoArray: function (arraya, arrayb) {
        for (var i = 0; i < arraya.length && arrayb.indexOf(arraya[i]) >= 0; i++) {
        }
        return(i === arraya.length);
    },
    removeArray: function (arraya, arrayb) {
        for (var i = 0; i < arraya.length; i++) {
            var j = arrayb.indexOf(arraya[i]);
            if (j !== -1) {
                arrayb.splice(j, 1);
            }

        }
        return(arrayb);
    },
//Valida que cada parametro del ordenamiento cumpla las validaciones [campo,odenamiento]
    validate: function (schema, data) {
        if ((data.length > 2) || (data.length < 1)) {
            return false;
        } else if ((data.length === 2) && ((data[1] != 'DESC') && (data [1] != 'ASC'))) {
            return false;
        } else if (schema.indexOf(data[0]) < 0) {
            return false;
        } else {
            return true;
        }
    },
    generate: function (schema, info, array, position, Sequelize) {
        if (info.length === position) {
            return(array);
        } else {
            var mini = info[position].split(':');
            if (this.validate(schema, mini)) {
                if (mini[0] === 'count') {
                    mini[0] = Sequelize.literal('count');
                }
                array.push(mini);
                return(this.generate(schema, info, array, position + 1, Sequelize));
            } else {
                return false;
            }
        }
    },
//verifica que el numero sea un entero positivo tiene problemas con las comas ","  :(
    isIntegerPositive: function (numero) {
        if (!(isNaN(numero)) && (numero % 1 == 0) && (numero > 0)) {
            return true;
        } else {
            return false;
        }
    },
    //verifica que el numero sea un flotante positivo tiene problemas con las comas ","  :(
    isFloatPositive: function (numero) {
        if (!(isNaN(numero)) && (numero > 0)) {
            return true;
        } else {
            return false;
        }
    },
// el primer parametro es un array con los campos que se van a comvertir, el segundo parametro es los parametros del objeto de validaciones para convertir al tipo de dato correspondiente, eltercer parametro es el objeto, parseSchema(["uno","dos","tres","cuatro"],{uno: '[1,2]', seis:'holis', tres: '[1,2,3,4]' })
    parseArray: function (schema, validations, data) {
        var aux = new Array();
        for (var indice in data) {
            if (typeof (data[indice]) === "string") {
                if ((schema.indexOf(indice) > -1) && (isNaN(data[indice]))) {
                    aux = data[indice].replace("]", "").replace("[", "").split(",");
                    switch (validations[indice].items.type) {
                        case "number":
                            for (var i in aux) {
                                if (this.isFloatPositive(aux[i]))
                                    aux[i] = parseInt(aux[i]);
                            }
                            break;
                        case "integer":
                            for (var i in aux) {
                                if (this.isIntegerPositive(aux[i]))
                                    aux[i] = parseInt(aux[i]);
                            }
                            break;
                        default:
                    }
                    data[indice] = aux;
                }
            }
        }
        return data;
    },
    parseInteger: function (array, data) {
        for (var indice in array) {
            if (typeof (data[array[indice]]) !== "undefined") {
                data[array[indice]] = parseInt(data[array[indice]]);
            }
        }
        return data;
    },
    parseFlotante: function (array, data) {
        for (var indice in array) {
            if (typeof (data[array[indice]]) !== "undefined") {
                data[array[indice]] = parseFloat(data[array[indice]]);
            }
        }
        return data;
    },
//extrae de los parametros del esquema de validacion los campos de enviado en el segundo parametro    
    getCampSchemaInArray: function (schema, camp, value) {
        var vector = new Array();
        for (var indice in schema) {
            if (schema[indice][camp] == value) {
                vector.push(indice);
            }
        }
        return vector;
    },
    //acomoda los campos betwen en el objeto de busqueda
    betweenIntoObject: function (schema, data) {
        for (var indice in data) {
            if (schema.indexOf(indice) > -1) {
                data[indice] = {"between": data[indice].sort(function (a, b) {
                        return a - b
                    })};
            }
        }
        return data;
    },
    //se encriptan claves
    claveIntoObject: function (schema, data) {
        for (var indice in data) {
            if (schema.indexOf(indice) > -1) {
                data[indice] = crypto.createHmac('sha256', secret).update(data[indice]).digest('hex');
            }
        }
        return data;
    },
    stringAClave: function (data) {
        return crypto.createHmac('sha256', secret).update(data).digest('hex');
    },
    //se agregan las imagenes en la respuesta
    rellenarImagenes: function (schema, data, validaciones) {
        for (var indice in schema) {

            if (typeof (data[schema[indice]]) !== "string" || (typeof (data[schema[indice]]) === "string" && data[schema[indice]] === "[]") || (typeof (data[schema[indice]]) === "string" && data[schema[indice]] === "")) {
                data[schema[indice]] = [{"pequena": validaciones[schema[indice]].urlImagenDefectoPequeno,
                        "mediana": validaciones[schema[indice]].urlImagenDefectoMediano,
                        "grande": validaciones[schema[indice]].urlImagenDefectoGrande,
                    }];
            } else {
                try {
                    var datos = JSON.parse(data[schema[indice]]);
                } catch (err) {
                    var datos = [data[schema[indice]]]
                }
                if (datos.length > 0) {
                    var array = [];
                    for (var i in datos) {
                        array.push({
                            "pequena": strings.imagenes.url + strings.imagenes[validaciones[schema[indice]].urlImagen] + "P_" + datos[i],
                            "mediana": strings.imagenes.url + strings.imagenes[validaciones[schema[indice]].urlImagen] + "M_" + datos[i],
                            "grande": strings.imagenes.url + strings.imagenes[validaciones[schema[indice]].urlImagen] + "G_" + datos[i]
                        });
                    }
                    data[schema[indice]] = array;
                }
            }
        }
        return data;
    },
    formatearImagen: function (data, url, campo) {
        data.pequena = strings.imagenes.url + strings.imagenes[url] + "P_" + data[campo];
        data.mediana = strings.imagenes.url + strings.imagenes[url] + "M_" + data[campo];
        data.grande = strings.imagenes.url + strings.imagenes[url] + "G_" + data[campo];
        delete(data[campo]);
        return data;

    },
    //se encriptan claves
    correoIntoObject: function (schema, data) {
        for (var indice in data) {
            if ((schema.indexOf(indice) > -1) && (this.validarEmail(data[indice]) === false)) {
                return false;
            }
        }
        return true;
    },
    validarEmail: function (email) {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!expr.test(email)) {
            return false;
        } else {
            return true;
        }
    },
    //se encriptan claves
    permalinkIntoObject: function (schema, data, validaciones) {
        for (var indice in schema) {
            data[schema[indice]] = this.crearPermalik(data[validaciones[schema[indice]].copia]) + (Math.floor(Math.random() * (9999 - 1)) + 1).toString();
        }
        return data;
    },
//acomoda los campos like en el objeto de busqueda
    likeIntoObject: function (schema, data) {
        for (var indice in data) {
            if (schema.indexOf(indice) > -1) {
                data[indice] = {"like": "%" + data[indice] + "%"};
            }
        }
        return data;
    }
    ,
//procesa los errores generado por ajv y los regresa en algo mas facil de entender
    processErrorAjv: function (error) {
        var message;
        switch (error[0].keyword) {
            case "additionalProperties":
                message = "El parametro " + error[0].params.additionalProperty + " no esta permitido para esta consulta";
                break;
            case "type":
                message = "El parametro " + error[0].dataPath + " debe ser de tipo " + error[0].params.type;
                break;
            case "format":
                message = "El parametro " + error[0].dataPath + " debe ser de tipo " + error[0].params.format;
                break;
            default:
                message = "los parametos de la url tiene un error pero no se pudo determinar";
                break;
        }
        error[0].keyword === "additionalProperties"
        return message;
    }
    ,
//el puntero se lleva en el objeto
    objectToFormat: function (object, format, puntero, index) {
        var aux = "";
        for (var property in format) {
//  console.log(puntero);
            if (typeof (format[property]) === "object") {
// console.log(puntero);
// object
                puntero = this.objectToFormat(object, format[property], puntero.substring(0, index) + '"' + property + '":{}' + aux + puntero.substring(index), property.length + 4 + index);
                aux = ",";
            } else {
                var responseReformar = this.processType(object, property, format[property].split("_"), 0);
                JSON.stringify(responseReformar)
                if (typeof (responseReformar) === "string") {
                    puntero = puntero.substring(0, index) + '"' + property + '":"' + responseReformar.replace(/\r?\n?"/g, "") + '"' + aux + puntero.substring(index);
                } else {
                    puntero = puntero.substring(0, index) + '"' + property + '":' + JSON.stringify(responseReformar) + aux + puntero.substring(index);
                }
                aux = ",";
                delete object[property];
            }
        }
        if (index === 1) {
// console.log(puntero);
            var pun = JSON.parse(this.clearString(puntero));
            // console.log("jajajaja");
            Object.assign(object, pun);
            //  console.log("jojojojo");
            return(object);
        } else {
            return(puntero);
        }
    },
    //convierte una respuesta a un json
    respuestaArray: function (object) {
        var array = [];
        for (var aux in object) {
            array.push(object[aux].dataValues);
        }
        return array;
    },
    objectToFormatArray: function (object, format) {
        if (typeof(format) !== "undefined") {
            for (var indice in object) {
                 if( typeof(object[indice].dataValues) !== "undefined"){
                   object[indice].dataValues = this.rellenarImagenes(this.getCampSchemaInArray(format.schema.properties, "dato", "imagen"), object[indice].dataValues, format.schema.properties);
                if (typeof (format.reformat) !== "undefined") {
                    object[indice].dataValues = this.objectToFormat(object[indice].dataValues, format.reformat, "{}", 1);
                }  
                 }else{
                   object.dataValues = this.rellenarImagenes(this.getCampSchemaInArray(format.schema.properties, "dato", "imagen"), object.dataValues, format.schema.properties);
                if (typeof (format.reformat) !== "undefined") {
                    object.dataValues = this.objectToFormat(object.dataValues, format.reformat, "{}", 1);
                }  
                 }
                
            }
        }
        return(object);
    },
    objectReformatArray: function (object) {
        var data = [];
        for (var indice in object) {
            data.push(object[indice].dataValues);
        }
        return(data);
    },
    objectReformatArrayBusquedaFacil: function (object, clavePrimaria) {
        var data = {};
        for (var indice in object) {
            data[object[indice]] = object[indice].dataValues;
        }
        return(data);
    },
    crearPermalik: function (cadena) {
        return cadena.replace(/Ã|À|Á|Ä|Â|È|É|Ë|Ê|Ì|Í|Ï|Î|Ò|Ó|Ö|Ô|Ù|Ú|Ü|Û|ã|à|á|ä|â|è|é|ë|ê|ì|í|ï|î|ò|ó|ö|ô|ù|ú|ü|û|Ñ|ñ|Ç|ç| /g, function myFunction(x) {
            var mal = "AEIOUÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç ";
            var bien = "aeiouaaaaaeeeeiiiioooouuuuaaaaaeeeeiiiioooouuuunncc_";
            return bien.charAt((mal.indexOf(x)));
        });
    },
    processType: function (object, property, type, aux) {
        switch (type[aux]) {
            case "image":

                break;
            case "array":
                var array = JSON.parse(object[property]);
                object[property] = array;
                return this.processType(object, property, type, aux + 1);
                break;
            default:
                return (object[property]);
                break;
        }
    },
    clearString: function (string) {
        return(string.replace(/\r?\n?/g, ""));
    },
    armarJson: function (validador, campoRelacion, valorRelacion, camposEliminados, campoRelacionInterna, valorRelacionInterna) {
        var json = "{";
        camposEliminados = utils.permalinkIntoObject(utils.getCampSchemaInArray(validador, "dato", "permalink"), camposEliminados, validador);
        for (var indice in validador) {
            if (indice !== campoRelacion && indice !== campoRelacionInterna && typeof (camposEliminados[indice]) !== "undefined") {

                json = json + '"' + indice + '":"' + camposEliminados[indice] + '",';

            }
        }
        json = json + '"' + campoRelacion + '":"' + valorRelacion + '","' + campoRelacionInterna + '":"' + valorRelacionInterna + '"}';
        return json;
    },
    generarHash: function () {
        return (parseInt((Math.random() * (99999999 + 10000000) + 10000000)).toString(16))
    }
};
module.exports = utils;
