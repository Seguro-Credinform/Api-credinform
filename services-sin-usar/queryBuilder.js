var utils = require('../services/utils');
var modelos = require('../services/modelos');
var validadores = require('../services/validadores');
var Ajv = require('ajv');
var sequelize = require('../configs/database');
var Sequelize = require('sequelize');
var answers = require('../services/answers');
var ajv = new Ajv({useDefaults: true});
var _ = require('lodash');
var queryBuilder = {
//data = data que proporcina el usuario por la url
//schema = eschema de validaciones
//statics  = variables que se excluyen de los validadores porque son estaticas
//invalid = variables que son invalidas para la validaciones de filtros, pero que se validan en otros metodos
//res  =respuesta del express
//extras = parametros de los vailadadores que no son deseados para la consulta
    condicionales: function (data, schema, statics, invalid, extras, res) {
        for (aux in invalid) {
            delete data[invalid[aux]];
        }
        for (aux in extras) {
            if (data[extras[aux]]) {
                answers.badRequest("el parametro " + extras[aux] + " no esta permitido", res);
                return false;
            }
        }
        data = utils.parseArray(utils.getCampSchemaInArray(schema.properties, "type", "array"), schema.properties, data);
        var valid = ajv.validate(schema, data);
        //muestra el error en la validacion
        if (!valid) {
            answers.badRequest(utils.processErrorAjv(ajv.errors), res);
            return false;

        }
        data = Object.assign(data, statics);
        data = utils.betweenIntoObject(utils.getCampSchemaInArray(schema.properties, "query", "between"), data);
        data = utils.likeIntoObject(utils.getCampSchemaInArray(schema.properties, "query", "like"), data);
        return (data);
    },
    //Ejecuta las consultas
    ejecutarConsultaActualizarConCallback: function (model, parametros, condicion) {
        return   model.update(parametros, {
            where: condicion
        })
    },
    consultaGetSimple: function (model, res, conditions, atributes, order, limit) {
        var consulta = {attributes: atributes,
            where: conditions}
        if (typeof (order) !== "undefined") {
            consulta.order = order;
        }
        if (typeof (limit) !== "undefined") {
            consulta.limit = limit;
        }
        return model.findAndCountAll(consulta);
    },
//se utiliza cuando los campos requeridos en la consulta son muy pocos
    addProperty: function (schema, propertys, res) {
        if (propertys) {
            var data = propertys.split(",");
            if (utils.intoArray(data, schema)) {
                return (data);
            } else {
                answers.badRequest("solo se permiten los campos " + schema + " en el parametro fields", res);
                return false;
            }
        } else {
            return (schema);
        }
    },
//se utiliza cuando son muchos los campos mostrados en la consulta y resulta mas sencillo remover campos los posibles mostrados
    removeProperty: function (schema, remove, propertys, res) {
        schema = utils.removeArray(remove, schema);
        if (propertys) {
            var data = propertys.split(",");
            if (utils.intoArray(data, schema)) {
                return (data);
            } else {
                answers.badRequest("solo se permiten los campos " + schema + " en el parametro fields", res);
                return false;
            }
        } else {
            return (schema);
        }
        return (schema);
    },
//valida que el string que se recive por la url cumple todos los parametros para utilizarlos como ordenamiento
    order: function (schema, propertys, res) {
        if (propertys) {
            var value = utils.generate(schema, propertys.split(','), [], 0, Sequelize);
            if (value) {
                return (value);
            } else {
                answers.badRequest("solo se permiten los campos " + schema + " y parametros DESC y ASC den la forma campo:DESC en el parametro sort", res);
                return false;
            }
        } else {
            return ([]);
        }
    },
//page=pagina&perPage=resultados por pagina
//retorna un objeto con limit y offset, que es la paginacion
    page: function (page, perPage, res) {
        var limit = 10;
        var offset = 0;
        if ((perPage) && (utils.isIntegerPositive(perPage)) && (parseInt(perPage) < 100)) {
            limit = parseInt(perPage);
        } else if ((perPage) || (perPage === 0)) {
            answers.badRequest("El parametro perPage debe ser un numero positivo entre 1 y 100", res);
            return false;
        }
        if ((page) && (Number.isInteger(parseInt(page))) && (utils.isIntegerPositive(page) > 0)) {
            offset = ((parseInt(page)) - 1) * limit;
        } else if ((page) || (page === 0)) {
            answers.badRequest("El parametro page debe ser un numero positivo mayor a 0", res);
            return false;
        }
        return ({limit: limit, offset: offset});
    },
    //result=resultado de la consulta,page=objeto de la pagina que retorna esta misma clase,NumResult=numero de resultados en la consulta,res=parametro para poder responder
    //conntruye la respuesta cuando es ok
    response: function (result, page, NumResult, res, reformat) {
        numPage = Math.ceil(NumResult / page.limit);
        if (numPage < ((page.offset / page.limit) + 1))
        {
            if (numPage > 0) {
                answers.badRequest("El numero de Pagina ingresado " + ((page.offset / page.limit) + 1) + " es mayor al maximo permitido " + numPage + " para esta consulta", res);
            } else {
                answers.notResult(res);
            }
        } else {
            answers.success(utils.objectToFormatArray(result, reformat), NumResult, numPage, res);
        }
    },
    responseCrear: function (result, res) {


        answers.successCrear(result, res);

    },
    //se verifica la variable contar inmueble ingresada por url, esta solo puede decir true, false, undefine
    contarInmuebles: function (data, res) {
        if ((data) && (data === "true")) {
            return 2;
        } else if ((data) && (data === "false")) {
            return 1;
        } else if (!data) {
            return 1;
        } else {
            answers.badRequest("El parametro count debe ser un true o false", res);
            return false;
        }
    },
    //ejecuta el query
    //reformat formato de salida
    // eliminar cuando ya no se utilice
    executeGet: function (model, res, conditions, atributes, sort, count, page, group, reformat) {
        if (conditions && atributes && sort && page && count) {
            if (count === 2) {
                atributes.push([Sequelize.literal('COUNT(*)'), 'count']);
            }
            model.findAndCountAll((Object.assign({
                attributes: atributes,
                where: conditions,
                group: group,
                order: sort
            }, page)))
                    .then(function (result) {
                        queryBuilder.response(result.rows, page, result.count.length | result.count, res, reformat);
                    }).catch(function (err) {
                answers.badRequest(err);
            });
        }
    },
    //Ejecuta las consultas
    ejecutarConsultaCrearCallback: function (tabla, parametros, res, extras, schema, camposMostrados) {
        var objeto = Object.assign(parametros, extras);
        objeto = utils.claveIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "clave"), objeto);
        objeto = utils.permalinkIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "permalink"), objeto, schema.properties);
        objeto = utils.parseArray(utils.getCampSchemaInArray(schema.properties, "type", "array"), schema.properties, objeto);
        objeto = utils.parseInteger(utils.getCampSchemaInArray(schema.properties, "type", "integer"), objeto);
        objeto = utils.parseFlotante(utils.getCampSchemaInArray(schema.properties, "type", "number"), objeto);
        var primaryKey = utils.getCampSchemaInArray(schema.properties, "primary", "si");
        if (utils.correoIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "correo"), objeto)) {
            var lista = utils.getCampSchemaInArray(schema.properties, "relacional", "si");

            var eliminados = {};
            for (var aux in lista) {
                if (typeof (objeto[lista[aux]]) !== "object") {
                    lista.splice(aux, 1);
                } else {
                    for (var indi in validadores[schema.properties[lista[aux]]["modeloRelacion"]].schema.properties) {
                        if (typeof (objeto[indi]) !== "undefined") {
                            eliminados[indi] = objeto[indi];
                            delete objeto[indi];
                        }
                    }

                }

            }
            var valid = ajv.validate(schema, objeto);
            //muestra el error en la validacion
            if (!valid) {
                answers.badRequest(utils.processErrorAjv(ajv.errors), res);
                return false;

            }
            return modelos[tabla].create(objeto);

        } else {
            answers.badRequest("El correo esta malo", res);
        }
    },
    //Ejecuta las consultas
    ejecutarConsultaCrear: function (tabla, parametros, res, extras, schema, camposMostrados) {
        var objeto = Object.assign(parametros, extras);
        objeto = utils.claveIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "clave"), objeto);
        objeto = utils.permalinkIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "permalink"), objeto, schema.properties);
        objeto = utils.parseArray(utils.getCampSchemaInArray(schema.properties, "type", "array"), schema.properties, objeto);
        objeto = utils.parseInteger(utils.getCampSchemaInArray(schema.properties, "type", "integer"), objeto);
        objeto = utils.parseFlotante(utils.getCampSchemaInArray(schema.properties, "type", "number"), objeto);

        var primaryKey = utils.getCampSchemaInArray(schema.properties, "primary", "si");
        if (utils.correoIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "correo"), objeto)) {
            var lista = utils.getCampSchemaInArray(schema.properties, "relacional", "si");

            var eliminados = {};
            for (var aux in lista) {
                if (typeof (objeto[lista[aux]]) !== "object") {
                    lista.splice(aux, 1);
                } else {
                    for (var indi in validadores[schema.properties[lista[aux]]["modeloRelacion"]].schema.properties) {
                        if (typeof (objeto[indi]) !== "undefined") {
                            eliminados[indi] = objeto[indi];
                            delete objeto[indi];
                        }
                    }

                }

            }
            var valid = ajv.validate(schema, objeto);
            //muestra el error en la validacion
            if (!valid) {
                answers.badRequest(utils.processErrorAjv(ajv.errors), res);
                return false;

            }

            modelos[tabla].create(objeto)
                    .then(function (result) {
                        modelos[tabla].findOne({
                            attributes: camposMostrados,
                            order: primaryKey + " DESC"
                        })
                                .then(function (result) {
                                    for (var aux in lista) {
                                        for (var aux2 in eliminados[lista[aux]]) {
                                            queryBuilder.ejecutarConsultaCrearRelaciones(schema.properties[lista[aux]]["modeloRelacion"], JSON.parse(utils.armarJson(validadores[schema.properties[lista[aux]]["modeloRelacion"]].schema.properties, lista[aux], eliminados[lista[aux]][aux2], eliminados, schema.properties[lista[aux]]["campoRelacion"], result.dataValues[schema.properties[lista[aux]]["campoRelacion"]])));
                                        }
                                    }
                                    queryBuilder.response(result, 1, 1, res, validadores[tabla]);
                                });

                    }).catch(function (err) {
                //answers.badRequest(err.errors[0].message, res);
                answers.badRequest(err, res);
            })
        } else {
            answers.badRequest("El correo esta malo", res);
        }
    },
    ejecutarObtenerUltimo: function (modelo ,camposMostrados, primaryKey) {
    return modelo.findOne({
                            attributes: camposMostrados,
                            order: primaryKey + " DESC"
                        });
                    },
    //crea relaciones
    ejecutarConsultaCrearRelaciones: function (tabla, parametros) {
        modelos[tabla].create(parametros)
                .then(function (result) {
                    return true;
                    //queryBuilder.responseCrear(resultado, res);
                }).catch(function (err) {
            return false;
            //answers.badRequest(err.errors[0].message, res);
            //answers.backendError("No se puedieron crear todas las relaciones", res);
        })

    },
    //Ejecuta las consultas
    ejecutarConsultaActualizar: function (model, parametros, res, condicion, schema) {
        schema = schema || {};
        if (typeof (schema.properties) === "object") {
            for (var aux in parametros) {
                if ((typeof (schema.properties[aux]) === "undefined") || (typeof (schema.properties[aux]) === "undefined") || ((typeof (schema.properties[aux].actualizar) === "string") && (schema.properties[aux].actualizar === "no"))) {
                    answers.badRequest("el parametro " + aux + " no esta permitido", res);
                    return false;
                }
            }
        }

        if ((utils.correoIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "correo"), parametros)) === false) {
            answers.badRequest("El correo esta malo", res);
        }
        model.update(parametros, {
            where: condicion
        })
                .then(function (result) {
                    queryBuilder.responseCrear(result, res);

                }).catch(function (err) {
            answers.badRequest(err, res);
        })
    },
    //Ejecuta las consultas
    ejecutarConsultaActualizarCallback: function (model, parametros, res, condicion, schema) {
        schema = schema || {};
        if (typeof (schema.properties) === "object") {
            for (var aux in parametros) {
                if ((typeof (schema.properties[aux]) === "undefined") || (typeof (schema.properties[aux]) === "undefined") || ((typeof (schema.properties[aux].actualizar) === "string") && (schema.properties[aux].actualizar === "no"))) {
                    answers.badRequest("el parametro " + aux + " no esta permitido", res);
                    return false;
                }
            }
        }

        if ((utils.correoIntoObject(utils.getCampSchemaInArray(schema.properties, "dato", "correo"), parametros)) === false) {
            answers.badRequest("El correo esta malo", res);
        }
        return model.update(parametros, {
            where: condicion
        });
               
    },//Ejecuta las consultas
    ejecutarConsultaEliminar: function (model, res, condicion) {
        model.destroy({
            where: condicion})
                .then(function (result) {
                    queryBuilder.responseCrear(result, res);

                }).catch(function (err) {
            answers.badRequest(err, res);
        })
    },
    //Ejecuta las consultas
    ejecutarConsultaCallback: function (model, res, page, count, sort, atributes, conditions, group, validator, join) {

        if (conditions && atributes && sort && page && count) {
            if (count === 2) {
                atributes.push([Sequelize.literal('COUNT(*)'), 'count']);
            }
            var include = [];
            if (typeof (join.innerJoin) !== "undefined") {
                for (var indice in join.innerJoin) {
                    //console.log(validator.schema.properties[indice].modeloRelacion);
                    model.belongsTo(modelos[validator.schema.properties[indice].modeloRelacion], {foreignKey: validator.schema.properties[indice].campoRelacion, targetKey: validator.schema.properties[indice].campoRelacion, as: validator.schema.properties[indice].modeloRelacion});
                    include.push({model: modelos[validator.schema.properties[indice].modeloRelacion], as: validator.schema.properties[indice].modeloRelacion, where: JSON.parse('{"' + indice + '":"' + join.innerJoin[indice] + '"}'), required: true});
                    //conditions = Object.assign(conditions, JSON.parse('{"'+validator.schema.properties[indice].modeloRelacion+'.'+indice+'":"'+join.innerJoin[indice]+'"}'));

                }
            }else if(typeof (join.leftJoin) !== "undefined"){
                if(typeof (join.leftJoin) === "string"){
                    join.leftJoin = JSON.parse(join.leftJoin);
                    for (var indice in join.leftJoin) { 
                        console.log(join.leftJoin[indice]);
                    console.log(validator.schema.properties[join.leftJoin[indice]].modeloRelacion);
                    console.log(validator.schema.properties[join.leftJoin[indice]].campoRelacion);
                    console.log(validator.schema.properties[join.leftJoin[indice]].campoRelacion);
                    model.belongsTo(modelos[validator.schema.properties[join.leftJoin[indice]].modeloRelacion], {foreignKey: validator.schema.properties[join.leftJoin[indice]].campoRelacion, as: validator.schema.properties[join.leftJoin[indice]].modeloRelacion});
                    include.push({model: modelos[validator.schema.properties[join.leftJoin[indice]].modeloRelacion], as: validator.schema.properties[join.leftJoin[indice]].modeloRelacion, required: false});
                }
                }else{
                   for (var indice in join.leftJoin) { 
                    model.belongsTo(modelos[validator.schema.properties[indice].modeloRelacion], {foreignKey: validator.schema.properties[indice].campoRelacion, targetKey: validator.schema.properties[indice].campoRelacion, as: validator.schema.properties[indice].modeloRelacion});
                    include.push({model: modelos[validator.schema.properties[indice].modeloRelacion], as: validator.schema.properties[indice].modeloRelacion, where: JSON.parse('{"' + indice + '":"' + join.leftJoin[indice] + '"}'), required: false});
                }
                }
                
            }

           // console.log(join);
            //relaciones requeridas
            var interseccion = _.intersection(utils.getCampSchemaInArray(validator.schema.properties, "relacional", "si"), atributes);
            atributes = _.difference(atributes, interseccion);
           return model.findAndCountAll((Object.assign({
                include: include,
                attributes: atributes,
                where: conditions,
                group: group,
                order: sort
            }, page)));
        }
    },
    //Ejecuta las consultas
    ejecutarConsulta: function (model, res, page, count, sort, atributes, conditions, group, validator, join) {

        if (conditions && atributes && sort && page && count) {
            if (count === 2) {
                atributes.push([Sequelize.literal('COUNT(*)'), 'count']);
            }
            var include = [];
            if (typeof (join.innerJoin) !== "undefined") {
                for (var indice in join.innerJoin) {
                    //console.log(validator.schema.properties[indice].modeloRelacion);
                    model.belongsTo(modelos[validator.schema.properties[indice].modeloRelacion], {foreignKey: validator.schema.properties[indice].campoRelacion, targetKey: validator.schema.properties[indice].campoRelacion, as: validator.schema.properties[indice].modeloRelacion});
                    include.push({model: modelos[validator.schema.properties[indice].modeloRelacion], as: validator.schema.properties[indice].modeloRelacion, where: JSON.parse('{"' + indice + '":"' + join.innerJoin[indice] + '"}'), required: true});
                    //conditions = Object.assign(conditions, JSON.parse('{"'+validator.schema.properties[indice].modeloRelacion+'.'+indice+'":"'+join.innerJoin[indice]+'"}'));

                }
            }else if(typeof (join.leftJoin) !== "undefined"){
                if(typeof (join.leftJoin) === "string"){
                    join.leftJoin = JSON.parse(join.leftJoin);
                    for (var indice in join.leftJoin) { 
                        console.log(join.leftJoin[indice]);
                    console.log(validator.schema.properties[join.leftJoin[indice]].modeloRelacion);
                    console.log(validator.schema.properties[join.leftJoin[indice]].campoRelacion);
                    console.log(validator.schema.properties[join.leftJoin[indice]].campoRelacion);
                    model.belongsTo(modelos[validator.schema.properties[join.leftJoin[indice]].modeloRelacion], {foreignKey: validator.schema.properties[join.leftJoin[indice]].campoRelacion, as: validator.schema.properties[join.leftJoin[indice]].modeloRelacion});
                    include.push({model: modelos[validator.schema.properties[join.leftJoin[indice]].modeloRelacion], as: validator.schema.properties[join.leftJoin[indice]].modeloRelacion, required: false});
                }
                }else{
                   for (var indice in join.leftJoin) { 
                    model.belongsTo(modelos[validator.schema.properties[indice].modeloRelacion], {foreignKey: validator.schema.properties[indice].campoRelacion, targetKey: validator.schema.properties[indice].campoRelacion, as: validator.schema.properties[indice].modeloRelacion});
                    include.push({model: modelos[validator.schema.properties[indice].modeloRelacion], as: validator.schema.properties[indice].modeloRelacion, where: JSON.parse('{"' + indice + '":"' + join.leftJoin[indice] + '"}'), required: false});
                }
                }
                
            }

           // console.log(join);
            //relaciones requeridas
            var interseccion = _.intersection(utils.getCampSchemaInArray(validator.schema.properties, "relacional", "si"), atributes);
            atributes = _.difference(atributes, interseccion);
            model.findAndCountAll((Object.assign({
                include: include,
                attributes: atributes,
                where: conditions,
                group: group,
                order: sort
            }, page)))
                    .then(function (result) {
                        if (interseccion.length > 0) {
                            queryBuilder.consultaRelaciona(result, page, res, interseccion, validator, 0, 0);

                        } else {
                            queryBuilder.response(result.rows, page, result.count.length | result.count, res, validator);
                        }
                    });
        }
    },
    consultaRelaciona: function (result, page, res, interseccion, validator, indiceInterseccion, indiceRespuesta) {
        /*modelos[validator['modeloRelacion']].findAndCountAll({
         where: conditions})
         .then(function (resultado) {*/

        if (typeof (result.rows[indiceRespuesta]) === 'object') {

            var were = JSON.parse('{"' + validator.schema.properties[interseccion[indiceInterseccion]]['campoRelacion'] + '" : "' + result.rows[indiceRespuesta].dataValues[validator.schema.properties[interseccion[indiceInterseccion]]['campoRelacion']] + '"}')
        //  console.log(validator.schema.properties[interseccion[indiceInterseccion]]['modeloRelacion']);
           // console.log(validadores[validator.schema.properties[interseccion[indiceInterseccion]]['modeloRelacion']]);
            modelos[validator.schema.properties[interseccion[indiceInterseccion]]['modeloRelacion']].findAndCountAll({
                attributes: validator.schema.properties[interseccion[indiceInterseccion]]['propiedadesRelacion'],
                where: were})
                    .then(function (resultado) {
                        result.rows[indiceRespuesta].dataValues[interseccion[indiceInterseccion]] = utils.objectReformatArray(utils.objectToFormatArray(resultado.rows, validadores[validator.schema.properties[interseccion[indiceInterseccion]]['modeloRelacion']]));
                        if (interseccion.length > indiceInterseccion + 1) {
                            indiceInterseccion = indiceInterseccion + 1;
                        } else {
                            indiceInterseccion = 0;
                            indiceRespuesta = indiceRespuesta + 1;
                        }
                        queryBuilder.consultaRelaciona(result, page, res, interseccion, validator, indiceInterseccion, indiceRespuesta);
                    });
        } else {
            queryBuilder.response(result.rows, page, result.count.length | result.count, res, validator);
        }

        /* });*/
    },
    generarCategorias: function (nombre, res) {
        modelos[nombre].drop()
                .then(function (result) {
                    sequelize.query("CREATE TABLE `categorias_ramas` (`id_categoria_rama` int(11) NOT NULL, `id_categoria` int(11) NOT NULL, `id_categoria_padre` int(11) NOT NULL, `nombre` varchar(500) COLLATE utf8_bin NOT NULL, `rama` varchar(800) COLLATE utf8_bin NOT NULL, `permalink` varchar(500) COLLATE utf8_bin NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;").spread(function (results, metadata) {
                        sequelize.query("ALTER TABLE `categorias_ramas` ADD PRIMARY KEY (`id_categoria_rama`);").spread(function (results, metadata) {
                            sequelize.query("ALTER TABLE `categorias_ramas` MODIFY `id_categoria_rama` int(11) NOT NULL AUTO_INCREMENT;").spread(function (results, metadata) {
                                queryBuilder.generarCategoriasRecursivo(res);
                            })
                        })

                    })

                });
    }
    ,
    createDataBase: function (nombre, res, callback) {
        console.log("jojojo");
       sequelize.query("CREATE DATABASE " + nombre).spread(function (results, metadata) {
                   return callback(true);
                    });

    },
    generarCategoriasRecursivo: function (res) {
        modelos['categorias'].findAndCountAll({
            where: {}})
                .then(function (result) {
                    var ramas = {};
                    var asignados = 0;
                    while (asignados < (result.count.length | result.count)) {
                        var movimientos = 0;
                        for (var aux1 in result.rows) {

                            if (typeof (ramas[[result.rows[aux1].dataValues.idCategoria]]) === "undefined") {
                                if (result.rows[aux1].dataValues.idCategoriaPadre === 0) {
                                    ramas[result.rows[aux1].dataValues.idCategoria] = [{[result.rows[aux1].dataValues.idCategoria]: result.rows[aux1].dataValues.nombre}];
                                    modelos['categoriasRamas'].create({
                                        idCategoria: result.rows[aux1].dataValues.idCategoria,
                                        idCategoriasPadre: result.rows[aux1].dataValues.idCategoriaPadre,
                                        nombre: result.rows[aux1].dataValues.nombre,
                                        rama: JSON.stringify(ramas[result.rows[aux1].dataValues.idCategoria]),
                                        permalink: result.rows[aux1].dataValues.permalink
                                    });
                                    movimientos = movimientos + 1;
                                    asignados = asignados + 1;
                                } else if (typeof (ramas[result.rows[aux1].dataValues.idCategoriaPadre]) === "object") {
                                    ramas[result.rows[aux1].dataValues.idCategoria] = _.cloneDeep(ramas[result.rows[aux1].dataValues.idCategoriaPadre]);
                                    ramas[result.rows[aux1].dataValues.idCategoria].push({[result.rows[aux1].dataValues.idCategoria]: result.rows[aux1].dataValues.nombre});
                                    modelos['categoriasRamas'].create({
                                        idCategoria: result.rows[aux1].dataValues.idCategoria,
                                        idCategoriaPadre: result.rows[aux1].dataValues.idCategoriaPadre,
                                        nombre: result.rows[aux1].dataValues.nombre,
                                        rama: JSON.stringify(ramas[result.rows[aux1].dataValues.idCategoria]),
                                        permalink: result.rows[aux1].dataValues.permalink
                                    });
                                    asignados = asignados + 1;
                                    movimientos = movimientos + 1;
                                }
                            }
                        }
                        if (movimientos === 0) {
                            answers.successCrear("el arbol de categorias no esta bien construido", res);
                            break;
                        }
                    }
                    answers.successCrear(ramas, res);


                })
    },
    actulaizarCategoria: function (res, id, respuesta, status) {
        modelos['categorias'].findAndCountAll({
            where: {}})
                .then(function (result) {
                    var ramas = {};
                    var editar = [];
                    editar.push(parseInt(id));
                    var asignados = 0;
                    while (asignados < (result.count.length | result.count)) {
                        var movimientos = 0;
                        for (var aux1 in result.rows) {

                            if (typeof (ramas[[result.rows[aux1].dataValues.idCategoria]]) === "undefined") {
                                if (result.rows[aux1].dataValues.idCategoriaPadre === 0) {
                                    ramas[result.rows[aux1].dataValues.idCategoria] = [{[result.rows[aux1].dataValues.idCategoria]: result.rows[aux1].dataValues.nombre}];
                                    movimientos = movimientos + 1;
                                    asignados = asignados + 1;
                                    if (((editar.indexOf(parseInt(result.rows[aux1].dataValues.idCategoriaPadre)) > -1) || (editar.indexOf(parseInt(result.rows[aux1].dataValues.idCategoria)) > -1)) && (status === "actualizar")) {
                                        console.log("edito");
                                        editar.push(result.rows[aux1].dataValues.idCategoria);
                                        modelos['categoriasRamas'].update({
                                            idCategoriaPadre: result.rows[aux1].dataValues.idCategoriaPadre,
                                            nombre: result.rows[aux1].dataValues.nombre,
                                            rama: JSON.stringify(ramas[result.rows[aux1].dataValues.idCategoria]),
                                            permalink: result.rows[aux1].dataValues.permalink
                                        }, {
                                            where: {idCategoria: result.rows[aux1].dataValues.idCategoria}
                                        })
                                                .then(function (result) {
                                                    //  queryBuilder.responseCrear(respuesta, res);
                                                    //  return true;
                                                })
                                    }
                                } else if (typeof (ramas[result.rows[aux1].dataValues.idCategoriaPadre]) === "object") {
                                    ramas[result.rows[aux1].dataValues.idCategoria] = _.cloneDeep(ramas[result.rows[aux1].dataValues.idCategoriaPadre]);
                                    ramas[result.rows[aux1].dataValues.idCategoria].push({[result.rows[aux1].dataValues.idCategoria]: result.rows[aux1].dataValues.nombre});
                                    asignados = asignados + 1;
                                    movimientos = movimientos + 1;
                                    if (((editar.indexOf(parseInt(result.rows[aux1].dataValues.idCategoriaPadre)) > -1) || (editar.indexOf(parseInt(result.rows[aux1].dataValues.idCategoria)) > -1)) && (status === "actualizar")) {
                                        console.log("edito");
                                        editar.push(result.rows[aux1].dataValues.idCategoria);
                                        modelos['categoriasRamas'].update({
                                            idCategoriaPadre: result.rows[aux1].dataValues.idCategoriaPadre,
                                            nombre: result.rows[aux1].dataValues.nombre,
                                            rama: JSON.stringify(ramas[result.rows[aux1].dataValues.idCategoria]),
                                            permalink: result.rows[aux1].dataValues.permalink
                                        }, {
                                            where: {idCategoria: result.rows[aux1].dataValues.idCategoria}
                                        })
                                                .then(function (result) {
                                                    //  queryBuilder.responseCrear(respuesta, res);
                                                    // return true;
                                                })
                                    }
                                    //  var array = 
                                    //  var array = [result.rows[aux1].idCategoria] = result.rows[aux1].nombre;
                                }
                            }

                        }
                        console.log(ramas);
                        if (movimientos === 0) {
                            // answers.successCrear("el arbol de categorias no esta bien construido", res);
                            break;
                            return true;
                        }
                    }
                    if (status === "crear") {
                        modelos['categoriasRamas'].create({
                            idCategoria: result.rows[aux1].dataValues.idCategoria,
                            idCategoriaPadre: result.rows[aux1].dataValues.idCategoriaPadre,
                            nombre: result.rows[aux1].dataValues.nombre,
                            rama: JSON.stringify(ramas[result.rows[aux1].dataValues.idCategoria]),
                            permalink: result.rows[aux1].dataValues.permalink
                        }).then(function (result) {
                            queryBuilder.responseCrear(respuesta, res);

                        })
                    } else {
                        queryBuilder.responseCrear(respuesta, res);
                    }
                    //answers.successCrear(ramas, res);
                    // answers.successCrear("se han reiniciado las categorias con exito", res);

                })
    }
};
module.exports = queryBuilder;