var answers = {
    notFound: function (res) {
        res.status("404").set('x-message', "El recurso solicitado no se encuentra").json({status: "404", message: "El recurso solicitado no se encuentra"});
    },
    notResult: function (res) {
        res.status("404").set('x-message', "La busqueda no tiene ningun resultado").json({status: "404", message: "La busqueda no tiene ningun resultado"});
    },
    badRequest: function (error, res) {
        res.status("400").set('x-message', error).json({status: "400", message: error});
    /*console.log(res);
        res.json(error);*/
    },
    invalidCredentials: function (error, res) {
        res.status("401").set('x-message', error).json({status: "401", message: error});
    /*console.log(res);
        res.json(error);*/
    },
    insufficientPermissions: function (error, res) {
        res.status("403").set('x-message', error).json({status: "403", message: error});
    /*console.log(res);
        res.json(error);*/
    },
    backendError: function (error, res) {
        res.status("503").set('x-message', error).json({status: "503", message: error});
    /*console.log(res);
        res.json(error);*/
    },
    successResponse: function (mensaje, res) {
        res.status("200").set('x-message', mensaje).json({status: "200", message: mensaje});
    /*console.log(res);
        res.json(error);*/
    },
    success : function (result, numResult, page , res) {
     //   console.log(numResult);
       // res.status("200").append('X-Total-Count', numResult).json(result);
     res.status("200").set({
  'X-Total-Count': numResult,
  'X-Number-Page': page
}).json(result);
    }
    ,successCrear : function (result, res) {
     //   console.log(numResult);
       // res.status("200").append('X-Total-Count', numResult).json(result);
     res.status("200").json(result);
    }
}
module.exports = answers;
