var conectorSql = require('../services/conectorSql');
var Sequelize = require('sequelize');
var modelos = require('../services/modelos');
var helpers = require('../services/helpers');
var strings = require('../configs/strings');
var _ = require('lodash');



module.exports = {
    order: function (data, validaciones) {
        var respuesta = {};
        var aux = [];
        var array = data.split(',');
        for (var index in array) {
            aux = array[index].split(':');
            respuesta[aux[0]] = aux[1];
        }
        return respuesta;
    },
    busqueda: function (datos, database, callback) {
        var data = _.cloneDeep(datos);
        delete data.sort;
        delete data.page;
        delete data.perPage;
        var respuesta = {};
        //modelos[database]['_id'] = {type: Sequelize.STRING};
        for (var index in data) {
            if (typeof (modelos[database][index]) === "undefined") {
                return callback("el parametro " + index + " no es valido", {});
            } else {
                respuesta[index] = data[index];
            }
        }
        return callback(false, respuesta);
    },
    //funcion que procesa los get
    /*peticion
     * database:nombre de la base de datos
     * busqueda:filtros de busqueda
     * populate:joins
     * campos:campos de respuesta
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * resu: objeto con los resultados de la busqueda
     * NumResult: numero de registros que puede tener la peticion
     * numPage: numero de paginas que puede tener peticiones
     */
    obtener: function (database, busqueda, populate, campos, callback) {
        var paginate = 0;
        console.log(typeof (callback));
        if ((typeof (busqueda.perPage) === "string") || (typeof (busqueda.perPage) === "number")) {
            var limite = parseInt(busqueda.perPage);
        } else {
            var limite = 10;
        }
        if (typeof (busqueda.sort) === "string") {
            var orden = this.order(busqueda.sort, {});
        } else {
            var orden = {};
        }
        if (typeof(busqueda.page)!=="undefined") {
            paginate = (parseInt(busqueda.page) - 1) * limite;
        }else{
            paginate = 0;
        }
        this.busqueda(busqueda, database, function (err, resulta) {
            if (err) {
                return callback(err, [], 0, 0);
            } else {
                conectorSql[database].findAndCountAll(({
                    //attributes: populate,
                    where: resulta,
                    // order: orden,
                    limit: limite,
                    offset: paginate
                }))
                        .then(function (result) {
                            helpers.formatearResp(result.rows, database, function (ers, resu) {
                                helpers.contarPaginas(paginate, limite, result.count, function (error, NumResult, numPage) {
                                    if (error) {
                                        return callback(error, resu, 0, 0);
                                    } else {
                                        return callback(err, resu, NumResult, numPage);
                                    }
                                });

                            })
                            //queryBuilder.response(result.rows, page, result.count.length | result.count, res, reformat);
                        }).catch(function (err) {
                    console.log(err);
                    return callback(err, "", 0, 0);
                });
            }
        });
    }
    ,
    //funcion para crear registros
    /*peticion
     * database:nombre de la base de datos
     * datos:campos que se van a insertar
     * respuesta:campos de respuesta
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * resu: objeto con los resultados de la creacion
     */
    crear: function (database, datos, respuesta, callback) {
        conectorSql[database].create(datos)
                .then(function (result) {
                    conectorSql[database].findOne({
                        attributes: respuesta,
                        where: datos
                    })
                            .then(function (result) {
                                var salida = {};
                                for (var aux in respuesta) {
                                    if (typeof (result[respuesta[aux]]) !== "undefined") {
                                        salida[respuesta[aux]] = result[respuesta[aux]];
                                    }
                                }
                                helpers.formatearResp(salida, database, function (ers, resu) {
                                    return callback("", resu);
                                })
                            });

                }).catch(function (err) {
            //answers.badRequest(err.errors[0].message, res);
            answers.badRequest(err, res);
        })
    },
    //funcion para crear registros
    /*peticion
     * database:nombre de la base de datos
     * parametros: parametros de la consulta (where)
     * datos:campos que se van a insertar
     * respuesta:campos de respuesta
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * salida: objeto con los resultados de la creacion
     */
    actualizar: function (database, parametros, datos, respuesta, callback) {
        // Model.findOneAndUpdate(query, { name: 'jason borne' }, options, callback)
        conectorSql[database].update(datos, {
            where: parametros
        })
                .then(function (result) {
                    conectorSql[database].findOne({
                        attributes: respuesta,
                        where: parametros
                    })
                            .then(function (result) {
                                var salida = {};
                                for (var aux in respuesta) {
                                    if (typeof (result[respuesta[aux]]) !== "undefined") {
                                        salida[respuesta[aux]] = result[respuesta[aux]];
                                    }
                                }
                                helpers.formatearResp(salida, database, function (ers, resu) {
                                    return callback("", resu);
                                })
                            });

                }).catch(function (err) {
            return callback(err, null);
        })
    }
//valida que el string que se recive por la url cumple todos los parametros para utilizarlos como ordenamiento

}

