var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
preguntasFrecuentes: {
        titulo: {type: String, default: ""},
        resumen: {type: String, default: ""},
        texto: {type: String, default: ""},
        imagen: {type: String, default: ""},
        status: {type: Number, default: 1}
    },
["titulo","resumen","texto","imagen","status"]
        */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("preguntasFrecuentes", req.query, [],["titulo","resumen","texto","imagen","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("preguntasFrecuentes", {"_id": req.params.id}, [],["titulo","resumen","texto","imagen","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("preguntasFrecuentes", req.body, ["titulo","resumen","texto","imagen","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("preguntasFrecuentes", {"_id": req.params.id}, req.body, ["titulo","resumen","texto","imagen","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("preguntasFrecuentes", {"_id": req.params.id}, {"status": 0}, ["titulo","resumen","texto","imagen","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crearImagen: function (req, res) {
        try {
            imagenes.subir(req, res, "preguntasFrecuentes", req.params.id, function (imagen) {
                //queryBuilder.ejecutarConsultaActualizarCallback(modelos.categorias, {imagen: imagen[3]}, res, {idCategoria: req.params.id}, validadores.categorias.schema).then(function (result) {
                mongoBuilder.actualizar("preguntasFrecuentes", {"_id": req.params.id}, {imagen: imagen[3]}, ["titulo","resumen","texto","imagen","status"], function (err, result) {
                    //answers.successCrear({pequena: strings.imagenes.url + strings.imagenes.categorias + imagen[0], mediana: strings.imagenes.url + strings.imagenes.categorias + imagen[1], grande: strings.imagenes.url + strings.imagenes.categorias + imagen[2]}, res);
                    answers.successCrear(result, res);
                });
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
        // answers.badRequest(imagen[3], res);

    },
    eliminarFisico: function (req, res) {
        try {
            mongoBuilder.eliminar("preguntasFrecuentes", {"_id": req.params.id}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    limpiar: function (req, res) {
        try {
            mongoBuilder.eliminar("preguntasFrecuentes", {}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
