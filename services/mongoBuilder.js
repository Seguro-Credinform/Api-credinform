var conectorMongo = require('../services/conectorMongo');
var modelos = require('../services/modelos');
var helpers = require('../services/helpers');
var strings = require('../configs/strings');
var _ = require('lodash');


module.exports = {
    order: function (data, validaciones) {
        var respuesta = {};
        var aux = [];
        var array = data.split(',');
        for (var index in array) {
            aux = array[index].split(':');
            respuesta[aux[0]] = aux[1];
        }
        return respuesta;
    },
    busqueda: function (datos, database, callback) {
        var data = _.cloneDeep(datos);
        delete data.sort;
        delete data.page;
        delete data.perPage;
        var respuesta = {};
        modelos[database]['_id'] = {type: "String"};
        for (var index in data) {

            if (typeof (modelos[database][index.split(".")[0]]) === "undefined") {
                return callback("el parametro " + index + " no es valido", {});
            } else {
                respuesta[index] = data[index];
            }
        }
        return callback(false, respuesta);
    },
    //funcion que procesa los get
    /*peticion
     * database:nombre de la base de datos
     * busqueda:filtros de busqueda
     * populate:joins
     * campos:campos de respuesta
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * resu: objeto con los resultados de la busqueda
     * NumResult: numero de registros que puede tener la peticion
     * numPage: numero de paginas que puede tener peticiones
     */
    obtener: function (database, busqueda, populate, campos, callback) {
        var paginate = 0;
        console.log(typeof (callback));
        if ((typeof (busqueda.perPage) === "string") || (typeof (busqueda.perPage) === "number")) {
            var limite = parseInt(busqueda.perPage);
        } else {
            var limite = 1500;
        }
        if (typeof (busqueda.sort) === "string") {
            var orden = this.order(busqueda.sort, {});
        } else {
            var orden = {};
        }
        if (typeof (busqueda.page) !== "undefined") {
            paginate = (parseInt(busqueda.page) - 1) * limite;
        } else {
            paginate = 0;
        }
        this.busqueda(busqueda, database, function (err, resulta) {
            if (err) {
                return callback(err, []);
            } else {
                conectorMongo[database].find(resulta, campos)
                        .sort(orden)
                        .limit(limite)
                        .skip(paginate)
                        .populate(populate)
                        .exec(function (err, result) {
                            helpers.formatearResp(result, database, function (ers, resu) {
                                conectorMongo[database].count(resulta, function (erer, count) {
                                    helpers.contarPaginas(paginate, limite, count, function (error, NumResult, numPage) {
                                        if (error) {
                                            return callback(error, resu, 0, 0);
                                        } else {
                                            return callback(err, resu, NumResult, numPage);
                                        }
                                    });

                                });
                            })
                        });
            }
        });
    },
    //funcion para crear registros
    /*peticion
     * database:nombre de la base de datos
     * datos:campos que se van a insertar
     * respuesta:campos de respuesta
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * resu: objeto con los resultados de la creacion
     */
    crear: function (database, datos, respuesta, callback) {
        conectorMongo[database].create(datos, function (err, result) {
            var salida = {};
            for (var aux in respuesta) {
                if (typeof (result[respuesta[aux]]) !== "undefined") {
                    salida[respuesta[aux]] = result[respuesta[aux]];
                }
            }
            helpers.formatearResp(salida, database, function (ers, resu) {
                return callback(null, resu);
            })
            //return callback("", salida);
        });
    },
    //funcion para crear registros
    /*peticion
     * database:nombre de la base de datos
     * parametros: parametros de la consulta (where)
     * datos:campos que se van a insertar
     * respuesta:campos de respuesta
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * salida: objeto con los resultados de la actualizacion
     */
    actualizar: function (database, parametros, datos, respuesta, callback) {
        // Model.findOneAndUpdate(query, { name: 'jason borne' }, options, callback)
        conectorMongo[database].findOneAndUpdate(parametros, datos, function (err, result) {
            if (err) {
                return callback(err, null);
            } else {
                var salida = {};
                for (var aux in respuesta) {
                    if (typeof (result[respuesta[aux]]) !== "undefined") {
                        salida[respuesta[aux]] = result[respuesta[aux]];
                    }
                    if (typeof (datos[respuesta[aux]]) !== "undefined") {
                        salida[respuesta[aux]] = datos[respuesta[aux]];
                    }
                }
                return callback(null, salida);
            }

        });
    },
    //funcion para crear registros
    /*peticion
     * database:nombre de la base de datos
     * parametros: parametros de la consulta (where)
     * datos:campos que se van a insertar
     * respuesta:campos de respuesta
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * salida: objeto con los resultados de la actualizacion
     */
    actualizarConFormateo: function (database, parametros, datos, respuesta, callback) {
        // Model.findOneAndUpdate(query, { name: 'jason borne' }, options, callback)
        conectorMongo[database].findOneAndUpdate(parametros, datos, function (err, result) {
            if (err) {
                return callback(err, null);
            } else {
                var salida = {};
                salida["_doc"] = {};
                for (var aux in respuesta) {
                    if (typeof (result[respuesta[aux]]) !== "undefined") {
                        salida["_doc"][respuesta[aux]] = result[respuesta[aux]];
                    }
                    if (typeof (datos[respuesta[aux]]) !== "undefined") {
                        salida["_doc"][respuesta[aux]] = datos[respuesta[aux]];
                    }
                }
                helpers.formatearResp(result, database, function (ers, resu) {
                    if (ers) {
                        return callback(ers, resu);
                    } else {
                        return callback(ers, resu);
                    }
                })
                // return callback(null, salida);
            }

        });
    },
    //funcion para eliminar registros
    /*peticion
     * database:nombre de la base de datos
     * parametros: parametros de la consulta (where)
     * callback: funcion de respuesta
     * respuesta
     * err: error
     * result: objeto con los resultados de la eliminacion
     */
    eliminar: function (database, parametros, callback) {
        conectorMongo[database].remove(parametros, function (err) {
            return callback(err);
        });
    }
//valida que el string que se recive por la url cumple todos los parametros para utilizarlos como ordenamiento

}