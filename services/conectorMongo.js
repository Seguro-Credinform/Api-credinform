'use strict';

var mongoose = require('mongoose');
var modelos = require('./modelos');
var uniqueValidator = require('mongoose-unique-validator');

module.exports = {
    sesiones: mongoose.model('sesiones', new mongoose.Schema(modelos.sesiones)),
    usuarios: mongoose.model('usuarios', new mongoose.Schema(modelos.usuarios).plugin(uniqueValidator)),
    documentos: mongoose.model('documentos', new mongoose.Schema(modelos.documentos)),
    siniestros: mongoose.model('siniestros', new mongoose.Schema(modelos.siniestros)),
    ubicaciones: mongoose.model('ubicaciones', new mongoose.Schema(modelos.ubicaciones)),
    preguntasFrecuentes: mongoose.model('preguntasFrecuentes', new mongoose.Schema(modelos.preguntasFrecuentes)),
    sugerencias: mongoose.model('sugerencias', new mongoose.Schema(modelos.sugerencias)),
    chats: mongoose.model('chats', new mongoose.Schema(modelos.chats)),
    mensajes: mongoose.model('mensajes', new mongoose.Schema(modelos.mensajes)),
    serviciosAdicionales: mongoose.model('serviciosAdicionales', new mongoose.Schema(modelos.serviciosAdicionales)),
    serviciosAdicionalesSolicitudes: mongoose.model('serviciosAdicionalesSolicitudes', new mongoose.Schema(modelos.serviciosAdicionalesSolicitudes))
};

