var io = require('socket.io').listen(8000);
var mongoBuilder = require('./../services/mongoBuilder');
var conecciones = [];
var metodos = {
    nuevaConexion: function (id) {
        conecciones.push({ "chat": "", "id": id, "emisor": 0 });
    },
    eliminarConexion: function (id) {
        for (var aux in conecciones) {
            if (conecciones[aux].id === id) {
                conecciones.splice(aux, 1);
            }
        }
    },
    asociasUsuario: function (usuario, id) {
        for (var aux in conecciones) {
            if (conecciones[aux].id === id) {
                conecciones[aux].usuario = usuario;
            }
        }
    },
    asociarConversacion: function (chat, emisor, id) {
        for (var aux in conecciones) {
            if (conecciones[aux].id === id) {
                conecciones[aux].chat = chat;
                conecciones[aux].emisor = emisor;
            }
        }
    },
    miUsuario: function (id) {
        for (var aux in conecciones) {
            if (conecciones[aux].id === id) {
                return conecciones[aux];
            }
        }
    },
    obtenerUsuario: function (usuario) {
        var array = [];
        for (var aux in conecciones) {
            if (conecciones[aux].usuario === usuario) {
                array.push(conecciones[aux].id);
            }
        }
        return array;
    }
}
io.on('connection', function (socket) {
    metodos.nuevaConexion(socket.id);
    io.emit('conectados', conecciones);
    // console.log('a user connected: ' + socket.id);
    //console.log(conecciones);
    socket.on('mensaje', function (data) {
        var usuario = metodos.miUsuario(socket.id);
        try {
            mongoBuilder.crear("mensajes", { "chat": usuario.chat, "tipo": 1, "mensaje": data, "emisor": usuario.emisor }, ["_id", "chat", "tipo", "mensaje", "emisor", "fecha"], function (err, result) {
                for (var aux in conecciones) {

                    if (conecciones[aux].chat === usuario.chat) {
                        io.to(conecciones[aux].id).emit('respuesta', JSON.stringify({ "emisor": result[0].emisor, "mensaje": result[0].mensaje, "fecha": result[0].fecha }));
                    }

                }
            });
        } catch (err) {
            io.to(socket.id).emit('{"emisor":3,"mensaje":"Ha ocurrido un problema al intentar enviar su mensaje intentelo en un minuto, disculpe las molestias","fecha":"00-00-000"}');
        }
        //io.emit('respuesta',metodos.miUsuario(socket.id).usuario +": "+ data);
        // io.emit('respuesta',metodos.miUsuario(socket.id).usuario +": "+ data.mensaje);
    });
    socket.on('login', function (data) {
        console.log(data);
        var datos = JSON.parse(data);
        metodos.asociarConversacion(datos.chat, datos.emisor, socket.id);
    });
    socket.on('cerrar', function (chat) {
        for (var aux in conecciones) {
            if (conecciones[aux].chat === chat) {
                io.to(conecciones[aux].id).emit('cerrar', "cerrar");
            }
        }
    });
    socket.on('disconnect', function () {
        metodos.eliminarConexion(socket.id);
        console.log('user disconnected');
        io.emit('conectados', conecciones);
    });

});
/*
 mensajes: {
 chat: {type: String, ref: 'chats', dato: ""},
 tipo: {type: Number, default: 1, dato: ""},//1:texto,2:audio,3:imagen
 mensaje: {type: String, default: "", dato: ""},
 emisor: {type: Number, default: 1, dato: ""}, //1 cliente, 2 soporte,3 error
 fecha: {type: Date, default: Date.now, dato: ""}
 } 
 */