/* global models */
var conectorMongo = require('../services/conectorMongo');
var mongoBuilder = require('../services/mongoBuilder');
var tmp = require('../services/tmp');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var answers = require('../services/answers');
var httpHelpers = require('../services/httpHelpers');
var strings = require('../configs/strings');
var moment = require('moment');
var secret = '5cuguvZ4Az';
module.exports = {
    createKey: function () {
        return crypto.createHmac('sha256', strings.sesion.hasClave).update(Math.floor(Math.random() * 16777215).toString(16) + "").digest('hex');
    },
    verificarLlave: function (req, res, next) {
        if (typeof (req.headers['x-access-llave']) === "string") {
            var llave = req.headers['x-access-llave'];
            conectorMongo.llaves.count((Object.assign({
                where: {status: 1, llave: llave}
            })))
                    .then(function (result) {
                        if (result < 1) {
                            answers.invalidCredentials('llave de acceso no valida', res);
                        } else {
                            next();
                        }
                        //queryBuilder.response(result.rows, page, result.count.length | result.count, res, reformat);
                    });
        } else {
            answers.insufficientPermissions('se necesita una llave de acceso', res);
        }
    },
    autenticacion: function (req, res, next) {
        // check header or url parameters or post parameters for token
        //var token = req.body.token || req.query.token || req.headers['x-access-token'];
        try {
            var token = req.headers['x-access-token'];
            if (token) {
                // verifies secret and checks exp
                jwt.verify(token, strings.sesion.hasEncriptacion, function (err, decoded) {
                    if (err) {
                        answers.invalidCredentials('token no valido', res);
                    } else {
                        tmp.usuarioActual._id = decoded._id;
                        tmp.usuarioActual.usuario = decoded.usuario;
                        mongoBuilder.actualizar("sesiones", {"_id": tmp.usuarioActual._id}, {"fecha": moment().toDate()}, ["_id", "info", "fecha"], function (errr, result) {
                            tmp.usuarioActual.info = result.info;
                            var tokenn = jwt.sign({"_id": tmp.usuarioActual._id, "usuario": tmp.usuarioActual.usuario}, strings.sesion.hasEncriptacion, {expiresIn: strings.sesion.duracion});
                            tmp.token = tokenn;
                            next();
                        });
                    }
                });

            } else {
                answers.insufficientPermissions('se necesita un token', res);
            }
        } catch (err) {
            console.log(err);
            console.log("error in server");
        }
    },
    renovarSesion: function (req, res) {
        // check header or url parameters or post parameters for token
        //var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (typeof (req.body.sesion) === "string") {
            //conectorMongo.users.find({'email': req.body.email, 'password': pass, '$or': [{'status': 1}, {'status': 2}]}).exec(function (err, result) {
            conectorMongo.sesiones.find({'_id': req.body.sesion}).exec(function (err, resulta) {
                //  console.log(result);
                if ((resulta.length) === 0) {
                    answers.invalidCredentials('Esta sesion no existe', res);
                } else {
                    var token = jwt.sign({"_id": resulta[0]["_id"], "usuario": resulta[0]["info"]["Usuario"]["NumeroAsegurado"]}, strings.sesion.hasEncriptacion, {expiresIn: strings.sesion.duracion});
                    answers.successCrear({token: token}, res);
                }
            });
        } else {
            answers.badRequest('Tiene que enviar una sesion por post', res);
        }
    },
    eliminarSesion: function (req, res) {
        // check header or url parameters or post parameters for token
        //var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (typeof (req.body.sesion) === "string") {
            //conectorMongo.users.find({'email': req.body.email, 'password': pass, '$or': [{'status': 1}, {'status': 2}]}).exec(function (err, result) {
            mongoBuilder.eliminar("sesiones", {"_id": req.body.sesion}, function (err) {
                if (err) {
                    answers.invalidCredentials('Esta sesion no existe', res);
                } else {
                    answers.successResponse("Eliminada sesion", res);
                }
            });
        } else {
            answers.badRequest('Tiene que enviar una sesion por post', res);
        }
    },
    reciclarSesiones: function (req, res) {
        try {
            // check header or url parameters or post parameters for token
            //var token = req.body.token || req.query.token || req.headers['x-access-token'];
            if (typeof (req.body.sesion) === "string") {
                //conectorMongo.users.find({'email': req.body.email, 'password': pass, '$or': [{'status': 1}, {'status': 2}]}).exec(function (err, result) {
                mongoBuilder.eliminar("sesiones", {"ambiente": "web", "fecha": {$lt: moment().subtract('seconds', strings.sesion.tiempoExpiracion).toDate()}}, function (err) {
                    if (err) {
                        console.log('Esta sesion no existe');
                    } else {
                        console.log("Eliminada sesion");
                    }
                });
            } else {
                aconsole.log('Tiene que enviar una sesion por post', res);
            }
        } catch (err) {
            console.log(err);
            console.log("error in server");
        }
    },
   /* generarToken: function (req, res) {
        if ((typeof (req.body.nTelefono) === "string") && (typeof (req.body.usuario) === "string")) {
            //conectorMongo.users.find({'email': req.body.email, 'password': pass, '$or': [{'status': 1}, {'status': 2}]}).exec(function (err, result) {
            httpHelpers.obtenerUsuario(req.body.nTelefono, req.body.usuario, function (err, result) {
                //  console.log(result);
                if (result === "") {
                    answers.invalidCredentials('nTelefono o usuario invalido', res);
                } else {
                        result.Usuario.telefono = req.body.nTelefono;
                    if (typeof (req.body.ambiente) === "string") {
                        var objeto = {info: result, ambiente: req.body.ambiente, email: req.body.nTelefono, password: req.body.usuario};
                    } else {
                        var objeto = {info: result, nTelefono: req.body.nTelefono, usuario: req.body.usuario};
                    }
                    mongoBuilder.crear("sesiones", objeto, ["_id", "ambiente", "info", "fecha"], function (error, respuesta) {
                        console.log("--------------------------------");
                        console.log(respuesta[0]);
                        console.log("--------------------------------");
                        var token = jwt.sign({"_id": respuesta[0]["_id"], "usuario": respuesta[0]["info"]["Usuario"]["NumeroAsegurado"]}, strings.sesion.hasEncriptacion, {expiresIn: strings.sesion.duracion});
                        result.token = token;
                        console.log(respuesta[0]["ambiente"]);
                        if (respuesta[0]["ambiente"] !== "web") {
                            result.sesion = respuesta[0]["_id"];
                        }
                        answers.successCrear(result, res);
                    });
                }
            });
        } else {
            answers.badRequest('Tiene que enviar nTelefono y usuario por post', res);
        }
    },*/
    generarToken: function (req, res) {
        if ((typeof (req.body.nTelefono) === "string") && (typeof (req.body.usuario) === "string")) {
            //conectorMongo.users.find({'email': req.body.email, 'password': pass, '$or': [{'status': 1}, {'status': 2}]}).exec(function (err, result) {
         //   httpHelpers.obtenerUsuario(req.body.nTelefono, req.body.usuario, function (err, result) {
                var result = {};
                result.Usuario = {};
                result.Usuario.NumeroAsegurado = "12345";
                //  console.log(result);
                if (result === "") {
                    answers.invalidCredentials('nTelefono o usuario invalido', res);
                } else {
                        result.Usuario.telefono = req.body.nTelefono;
                    if (typeof (req.body.ambiente) === "string") {
                        var objeto = {info: result, ambiente: req.body.ambiente, email: req.body.nTelefono, password: req.body.usuario};
                    } else {
                        var objeto = {info: result, nTelefono: req.body.nTelefono, usuario: req.body.usuario};
                    }
                    mongoBuilder.crear("sesiones", objeto, ["_id", "ambiente", "info", "fecha"], function (error, respuesta) {
                        console.log("--------------------------------");
                        console.log(respuesta[0]);
                        console.log("--------------------------------");
                        var token = jwt.sign({"_id": respuesta[0]["_id"], "usuario": respuesta[0]["info"]["Usuario"]["NumeroAsegurado"]}, strings.sesion.hasEncriptacion, {expiresIn: strings.sesion.duracion});
                        result.token = token;
                        console.log(respuesta[0]["ambiente"]);
                        if (respuesta[0]["ambiente"] !== "web") {
                            result.sesion = respuesta[0]["_id"];
                        }
                        answers.successCrear(result, res);
                    });
                }
         //   });
        } else {
            answers.badRequest('Tiene que enviar nTelefono y usuario por post', res);
        }
    },
    autenticacionAdmin: function (req, res, next) {
        // check header or url parameters or post parameters for token
        //var token = req.body.token || req.query.token || req.headers['x-access-token'];
         next();
       /* var token = req.headers['x-access-token'];
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, strings.sesion.hasEncriptacion, function (err, decoded) {
                if (err) {
                    answers.invalidCredentials('token no valido', res);
                } else {
             //       console.log(decoded);
                    tmp.usuarioActual._id = decoded._id;
               //     console.log(tmp);
                    var tokenn = jwt.sign({"_id": tmp.usuarioActual._id}, strings.sesion.hasEncriptacion, {expiresIn: strings.sesion.duracion});
                    tmp.token = tokenn;
                    next();
                }
            });

        } else {
            answers.insufficientPermissions('se necesita un token', res);
        }*/
    },
    generarTokenAdmin: function (req, res) {
        try {
            if ((typeof (req.body.email) === "string") && (typeof (req.body.password) === "string")) {
                var pass = crypto.createHmac('sha256', strings.sesion.hasClave)
                        .update(req.body.password)
                        .digest('hex');
                mongoBuilder.obtener("usuarios", {'email': req.body.email, 'password': pass,'status': 1}, [], ["_id", "email", "nombre","tipo"], function (err, result, NumResult, numPage) {
                    //conectorMongo.users.find({'email': req.body.email, 'password': pass, '$or': [{'status': 1}, {'status': 2}]}).exec(function (err, result) {
                    console.log(result);
                    if (result.length < 1) {
                        answers.invalidCredentials('email o contraseña invalido', res);
                    } else {
                        var token = jwt.sign({"_id": result[0]["_id"]}, strings.sesion.hasEncriptacion, {expiresIn: strings.sesion.duracion});
                        result[0].password = undefined;
                        result[0]["_doc"].token = token;
                        answers.successCrear(result[0], res);
                    }
                });
            } else {
                answers.badRequest('Tiene que enviar email y password por post', res);
            }
        } catch (err) {
            console.log(err);
            console.log("error in server");
        }

    },
    nuevoUsuario: function (req, res) {
        try {
            console.log("hola");
            if ((typeof (req.body.email) === "string") && (typeof (req.body.password) === "string") && (typeof (req.body.nombre) === "string")) {
                var hash = crypto.createHmac('sha256', strings.sesion.hasClave)
                        .update(req.body.password)
                        .digest('hex');
                console.log("hola");
                mongoBuilder.obtener("usuarios", {'email': req.body.email}, [], ["_id"], function (err, resulta, NumResult, numPage) {
                //conectorMongo.usuarios.find({'email': req.body.email}).exec(function (err, resulta) {
                    console.log("hola");
                    if ((resulta.length) > 0) {
                        answers.invalidCredentials('el email ya esta registrado', res);
                    } else {
                        console.log("hola");
                        var newUser = {"email": req.body.email, "password": hash, "nombre": req.body.nombre};
                        mongoBuilder.crear("usuarios", newUser, ["_id", "email","nombre",,"tipo"], function (err, result) {
                            if (err) {
                                console.log(err);
                                answers.badRequest('Error al intentar crear el nuevo usuario', res);
                            } else {
                                var token = jwt.sign({"_id": result["_id"]}, strings.sesion.hasEncriptacion, {expiresIn: strings.sesion.duracion});
                                result[0].token = token;
                                answers.successCrear(result, res);
                            }

                        })

                    }
                });
            } else {
                answers.badRequest('Tiene que enviar email, country y password por post', res);
            }
        } catch (err) {
            console.log(err);
            console.log("error in server");
        }
    }
};
