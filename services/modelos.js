'use strict';
var mongoose = require('mongoose');
module.exports = {
    sesiones: {
        info: {type: mongoose.Schema.Types.Mixed, dato: ""},
        ambiente: {type: String, default: "web", dato: ""},
        usuario: {type: String, default: "usuario", dato: ""},
        nTelefono: {type: String, default: "nTelefono", dato: ""},
        fecha: {type: Date, default: Date.now, dato: ""}
    },
    usuarios: {
        nombre: {type: String, required: true, dato: ""},
        tipo: {type: Number, default: 2, dato: ""},//1 admin, 2 operadora, 3 informacion
        email:  {type: String, required: true, unique: true, dato: ""},
        password:  {type: String, required: true, dato: ""},
        status: {type: Number, default: 1, dato: ""}
    },
    documentos: {
        nombre: {type: String, default: "", dato: ""},
        info: {type: String, default: "web", dato: ""},
        doc: {type: mongoose.Schema.Types.Mixed, dato: ""},
        status: {type: Number, default: 1, dato: ""}
    },
    siniestros: {
        tipoSiniestro: {type: String, default: "A", dato: ""},//A : Accidente,R : Robo
        fechaAccidente: {type: Date, required: true, dato: ""},
        fechaCreacion: {type: Date, default: Date.now, dato: ""},
        descripcion: {type: String, default: "", dato: ""},
        usuario: {type: mongoose.Schema.Types.Mixed, dato: ""},
        fotos: {type: [String], default: [], dato: "arrayimagen"},
        direccion: {type: String, default: "", dato: ""},
        latitud: {type: String, default: "", dato: ""},
        longitud: {type: String, default: "", dato: ""},
        status: {type: Number, default: 1, dato: ""} //1 en proceso de llenado 2 por atender y 3 atendido
    },
    ubicaciones: {
        nombre: {type: String, default: "", dato: ""},
        tipoUbicacion: {type: Number, dato: 1},//1: oficinas, 2: lugares de pago
        direccion: {type: String, default: "", dato: ""},
        telefonos: {type: [String], default: []}, dato: "",
        correo: {type: [String], default: [], dato: ""},
        horario: {type: String, default: "", dato: ""},
        latitud: {type: String, default: "", dato: ""},
        longitud: {type: String, default: "", dato: ""},
        status: {type: Number, default: 1, dato: ""}
    },
    preguntasFrecuentes: {
        titulo: {type: String, default: "", dato: ""},
        resumen: {type: String, default: "", dato: ""},
        texto: {type: String, default: "", dato: ""},
        imagen: {type: String, default: "", dato: "imagen"},
        status: {type: Number, default: 1, dato: ""}
    },
    sugerencias: {
        asunto: {type: String, default: "", dato: ""},
        tipoSugerencia: {type: Number, dato: 1},//1: sugerencia, 2: reclamo, 3:pregunta
        mensaje: {type: String, default: "", dato: ""},
        usuario: {type: mongoose.Schema.Types.Mixed, dato: ""},
        respuesta: {type: String, default: "", dato: ""},
        status: {type: Number, default: 1, dato: ""},
        fechaCreacion: {type: Date, default: Date.now, dato: ""},
    },
    chats: {
        usuario: {type: mongoose.Schema.Types.Mixed, dato: ""},
        fecha: {type: Date, default: Date.now, dato: ""},
        tipoChat: {type: Number, default: 1, dato: ""}, //1:atencion al cliente
        status: {type: Number, default: 1, dato: ""}
    },
    mensajes: {
        chat: {type: String, ref: 'chats', dato: ""},
        tipo: {type: Number, default: 1, dato: ""},//1:texto,2:audio,3:imagen
        mensaje: {type: String, default: "", dato: ""},
        emisor: {type: Number, default: 1, dato: ""}, //1 cliente, 2 soporte
        fecha: {type: Date, default: Date.now, dato: ""}
    },
    serviciosAdicionales: {
        nombre: {type: String, default: "", dato: ""},
        imagen: {type: String, default: "", dato: "imagen"},
        status: {type: Number, default: 1, dato: ""}
    },
    serviciosAdicionalesSolicitudes: {
        usuario: {type: mongoose.Schema.Types.Mixed, dato: ""},
        servicioAdicional: {type: String, ref: 'serviciosAdicionales', dato: ""},
        latitud: {type: String, default: "", dato: ""},
        longitud: {type: String, default: "", dato: ""},
        status: {type: Number, default: 1, dato: ""},
        fechaCreacion: {type: Date, default: Date.now, dato: ""},
    }
};


