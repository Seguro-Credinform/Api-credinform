var fs = require('fs');//necesario para subir archivos
var gm = require('gm').subClass({imageMagick: true});
module.exports = {
    subir: function (req, res, ubicacion, id, callback)
    {
        var b64string = req.body.media;
        var buf = new Buffer(b64string, 'base64'); // Ta-da
        var nombreImagen = this.getNewFileName("png", id);
        var finalPath = './archivos/' + ubicacion + '/' + nombreImagen[2];
        fs.writeFile(finalPath, buf, function (err) {
            if (err) {
                return res.status(500).send({code: 500, msg: 'Error Upload!'});
            }
            gm(finalPath)
                    .resize(50, 50)
                    .autoOrient()
                    .write('./archivos/' + ubicacion + '/' + nombreImagen[0], function (err) {
                        if (!err)
                            console.log(' hooray! ');
                        gm(finalPath)
                                .resize(500, 500)
                                .autoOrient()
                                .write('./archivos/' + ubicacion + '/' + nombreImagen[1], function (err) {
                                    if (!err) {
                                        console.log(' hooray! ');
                                    }
                                    return callback(nombreImagen, false);
                                });
                    });
        });
    },
    //obtenemos un número entero aleatorio para el nombre de la imagen
    getIntRandom: function (min, max)
    {
        return Math.floor((Math.random() * ((max + 1) - min)) + min);
    },
    //separamos entre el - y el .
    getBetweenSeparators: function (str)
    {
        return str.substring(str.lastIndexOf("-") + 1, str.lastIndexOf("."));
    },
    //obtiene el nuevo nombre del archivo si existe el anterior
    getNewFileName: function (file, id)
    {
        var f1 = this.getIntRandom(10, 10000000) + "." + file;
        var nombresito = this.getIntRandom(10, 10000000) + this.getBetweenSeparators(f1);
        return ["P_" + nombresito + "_" + id + "." + file, "M_" + nombresito + "_" + id + "." + file, "G_" + nombresito + "_" + id + "." + file, nombresito + "_" + id + "." + file];
    }

}

/*
 export function uploadImage(req, res) {
 
 
 
 var route = 'server/api/activity/media/medium-activity/' + nameImg + '.png';
 var routerThumb = 'server/api/activity/media/thumbnail-activity/' + nameImg + '.png';
 var responseImg = 'api/activity/image-response?name=medium-activity/' + nameImg + '.png';
 var responseThumb = 'api/activity/image-response?name=thumbnail-activity/' + nameImg + '.png';
 
 fs.writeFile(route, buf, function (err) {
 if (err) {
 return res.status(500).send({code: 500, msg: 'Error Upload!'});
 }
 
 gm(route)
 .resize(600, 400)
 .autoOrient()
 .write(route, function (errG) {
 if (errG) {
 return res.status(500).send({code: 500, msg: errG});
 }
 gm(route)
 .resize(100, 100)
 .autoOrient()
 .write(routerThumb, function (errGM) {
 if (errGM) {
 return res.status(500).send({code: 500, msg: errGM});
 }
 
 Activity.findById(req.params.id,
 function (err2, responde) {
 if (err2) {
 return res.status(500).send({code: 500, msg: err2});
 }
 return Activity.update({_id: req.params.id}, {
 $set: {
 'mediumUrl': responseImg,
 'thumbUrl': responseThumb
 }
 },
 function (err3) {
 if (err3) {
 return res.send(500, {code: 500, msg: err3});
 }
 res.status(200).send({code: 200, msg: responseImg});
 }
 );
 })
 });
 })
 
 });
 
 }*/


