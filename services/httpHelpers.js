var request = require('request');
var strings = require('../configs/strings');


module.exports = {
    obtenerUsuario: function (telefono, clave, callback) {
        request.post({url: strings.subApi+'/api/UsuarioPolizaO/ObtienePolizas', form: {nTelefono: telefono, usuario: clave.toUpperCase()}}, function optionalCallback(err, httpResponse, body) {
            return callback(err, JSON.parse(body));
        });
    }
}