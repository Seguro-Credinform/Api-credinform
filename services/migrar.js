var strings = require('../configs/strings');
//var _ = require('lodash');
var mysql = require('mysql2');
var conector = strings.mysql;
//var conectorDos = strings.mysqlDos;
//var fs = require('fs');
//var request = require('request');

var connecta = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password});
var tablas = {
    "emergencias": `CREATE TABLE emergencias (
  id_emergencia int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(200) COLLATE utf8_bin NOT NULL,
  telefono varchar(30) COLLATE utf8_bin NOT NULL,
  correo varchar(255) COLLATE utf8_bin DEFAULT '',
  imagen varchar(255) COLLATE utf8_bin DEFAULT '',
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_emergencia)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "informaciones": `CREATE TABLE informaciones (
                id_informacion int(11) NOT NULL AUTO_INCREMENT, 
                titulo varchar(300) COLLATE utf8_bin NOT NULL,
                descripcion text COLLATE utf8_bin NOT NULL,
                imagen varchar(255) COLLATE utf8_bin DEFAULT '',
                status int(11) NOT NULL DEFAULT '1',
                PRIMARY KEY (id_informacion)
               ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "ofertas": `CREATE TABLE ofertas (
  id_oferta int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(500) COLLATE utf8_bin NOT NULL,
  descripcion text COLLATE utf8_bin NOT NULL,
  imagen varchar(400) COLLATE utf8_bin DEFAULT '',
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_oferta)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "oficinas": `CREATE TABLE oficinas (
  id_oficina int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(200) COLLATE utf8_bin NOT NULL,
  correo varchar(255) COLLATE utf8_bin DEFAULT '',
  telefono varchar(30) COLLATE utf8_bin DEFAULT '',
  direccion text COLLATE utf8_bin NOT NULL,
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_oficina)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "llaves": `CREATE TABLE llaves (
  id_llave int(11) NOT NULL AUTO_INCREMENT,
  llave varchar(100) COLLATE utf8_bin NOT NULL,
  status int(1) NOT NULL DEFAULT '1',
  creacion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id_llave)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "sesiones": `CREATE TABLE sesiones (
  token varchar(500) COLLATE utf8_bin NOT NULL,
  informacion text COLLATE utf8_bin NOT NULL,
  usuario varchar(100) COLLATE utf8_bin NOT NULL,
  creacion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);`,
    "tipos_mensajes": `CREATE TABLE tipos_mensajes (
  id_tipo_mensaje int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(500) COLLATE utf8_bin NOT NULL,
  imagen varchar(400) COLLATE utf8_bin DEFAULT '',
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_tipo_mensaje)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "mensajes": `CREATE TABLE mensajes (
  id_mensaje int(11) NOT NULL AUTO_INCREMENT,
  id_tipo_mensaje int(11) NOT NULL,
  asunto varchar(500) COLLATE utf8_bin NOT NULL,
  mensaje text COLLATE utf8_bin NOT NULL,
  respuesta text COLLATE utf8_bin NULL,
  username varchar(500) COLLATE utf8_bin NOT NULL,
  creacion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_mensaje),
  KEY id_tipo_mensaje (id_tipo_mensaje),
  CONSTRAINT mensajes_ibfk_1 FOREIGN KEY (id_tipo_mensaje) REFERENCES tipos_mensajes (id_tipo_mensaje)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "mensajes_respuestas": `CREATE TABLE mensajes_respuestas (
  id_mensaje_respuesta int(11) NOT NULL AUTO_INCREMENT,
  id_mensaje int(11) NOT NULL,
  mensaje text COLLATE utf8_bin NOT NULL,
  username varchar(500) COLLATE utf8_bin NOT NULL,
  creacion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_mensaje_respuesta),
  KEY id_mensaje (id_mensaje),
  CONSTRAINT mensajes_respuestas_ibfk_1 FOREIGN KEY (id_mensaje) REFERENCES mensajes (id_mensaje)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "polizas": `CREATE TABLE polizas (
  id_poliza int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(500) COLLATE utf8_bin NOT NULL,
  imagen varchar(400) COLLATE utf8_bin DEFAULT '',
  descripcion text COLLATE utf8_bin NOT NULL,
  id_tipo_bien int(11) NOT NULL,
  id_riesgo int(11) NOT NULL,
  id_presupuesto int(11) NOT NULL,
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_poliza)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    "hospitales": `CREATE TABLE hospitales (
  id_hospital int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(500) COLLATE utf8_bin NOT NULL,
  telefono varchar(30) COLLATE utf8_bin DEFAULT '',
  direccion text COLLATE utf8_bin NOT NULL,
   correo text COLLATE utf8_bin NOT NULL,
  imagen varchar(400) COLLATE utf8_bin DEFAULT '',
  status int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (id_hospital)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`
};


var data = {
    llaves: `INSERT INTO llaves VALUES (1,'123456789',1,'2017-06-13 14:04:59');`,
    tipoUno: `INSERT INTO tipos_mensajes VALUES (1,'sugerencia');`,
    tipoDos: `INSERT INTO tipos_mensajes VALUES (2,'Reclamo');`,
    tipoTres: `INSERT INTO tipos_mensajes VALUES (3,'Preguntas');`
}
var consultas = [
    `CREATE TABLE sesiones (
  token varchar(500) COLLATE utf8_bin NOT NULL,
  informacion text COLLATE utf8_bin NOT NULL
);`,
    `CREATE TABLE llaves (
  id_llave int(11) NOT NULL AUTO_INCREMENT,
  llave varchar(100) COLLATE utf8_bin NOT NULL,
  status int(1) NOT NULL DEFAULT '1',
  creacion timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id_llave)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;`,
    `INSERT INTO llaves VALUES (1,'123456789',1,'2017-06-13 14:04:59');`
];




console.log("iniciando");

function cargarQuery(coneccion, indice, consultas, callback) {
    if (indice < consultas.length) {
        coneccion.query(consultas[indice], function () {
            console.log(consultas[indice]);
            cargarQuery(coneccion, indice + 1, consultas, callback);
        });
    } else {
        return callback(null, "fin");
    }
}


connecta.query('DROP DATABASE IF EXISTS ' + conector.database, function () {
    connecta.query('CREATE DATABASE ' + conector.database, function () {
        var connection = mysql.createConnection({host: conector.host, user: conector.user, password: conector.password, database: conector.database});
        cargarQuery(connection, 0, consultas, function (err, resp) {
            console.log(resp);
        });
    });
});
