var modelos = require('../services/modelos');
var strings = require('../configs/strings');
var mongoBuilder = require('../services/mongoBuilder');
var helpers = require('../services/helpers');
var _ = require('lodash');

module.exports = {
    //comprueba si existe un usuario
    comprobarUsuario: function (email ,callback){
       if(helpers.validarEmail(email)){
           mongoBuilder.obtener("users", {"email":email}, [], function(err, data){
               console.log(data.length);
               if ((err) || (data.length>0)){
                 return  callback(err, data);  
               }else{
                       return  callback("Email no registrado", data);
               }
        });
       }else{
           return  callback("El email no es valido",[]);
       }
        
    }
    
}