var modelos = require('../services/modelos');
var strings = require('../configs/strings');
var _ = require('lodash');

module.exports = {
    formatearResp: function (data, tabla, callback) {
        if (Object.prototype.toString.call(data) == '[object Array]') {
            var respuesta = data;
        } else {
            var respuesta = [];
            respuesta.push(data);
        }
        for (var aux in respuesta) {
            for (var indice in respuesta[aux]) {
                if ((typeof (modelos[tabla][indice]) !== "undefined") && (typeof (modelos[tabla][indice].dato) !== "undefined")) {
                    if (typeof (respuesta[aux]["_doc"]) !== "undefined") {
                        if (typeof (respuesta[aux]["_doc"][indice]) !== "undefined") {
                            switch (modelos[tabla][indice].dato) {
                                case "imagen":
                                    if (respuesta[aux]["_doc"][indice] !== "") {
                                        respuesta[aux]["_doc"][indice] = {
                                            pequena: strings.imagenes.url + strings.imagenes[tabla] + "P_" + respuesta[aux][indice],
                                            mediana: strings.imagenes.url + strings.imagenes[tabla] + "M_" + respuesta[aux][indice],
                                            grande: strings.imagenes.url + strings.imagenes[tabla] + "G_" + respuesta[aux][indice]
                                        }
                                    } else {
                                        respuesta[aux]["_doc"][indice] = {
                                            pequena: strings.imagenes.url + strings.imagenes[tabla] + "default.png",
                                            mediana: strings.imagenes.url + strings.imagenes[tabla] + "default.png",
                                            grande: strings.imagenes.url + strings.imagenes[tabla] + "default.png"
                                        }
                                    }
                                    break;
                                case "arrayimagen":
                                    //     console.log(respuesta[aux]["_doc"][indice]);
                                    var arrayAux = [];
                                       //   for (var indiimagen in respuesta[aux]["_doc"][indice]){
                                    for (var indiimagen = 0; indiimagen < respuesta[aux]["_doc"][indice].length; indiimagen++) {
                                        // console.log(indiimagen);
                                        arrayAux.push({
                                            pequena: strings.imagenes.url + strings.imagenes[tabla] + "P_" + respuesta[aux][indice][indiimagen],
                                            mediana: strings.imagenes.url + strings.imagenes[tabla] + "M_" + respuesta[aux][indice][indiimagen],
                                            grande: strings.imagenes.url + strings.imagenes[tabla] + "G_" + respuesta[aux][indice][indiimagen]
                                        });
                                    }
                                    respuesta[aux]["_doc"][indice+"Copy"] = _.cloneDeep(respuesta[aux]["_doc"][indice]);
                                    respuesta[aux]["_doc"][indice] = arrayAux;
                                    
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        if (typeof (respuesta[aux][indice]) !== "undefined") {
                            switch (modelos[tabla][indice].dato) {
                                case "imagen":
                                    if (respuesta[aux][indice] !== "") {
                                        respuesta[aux][indice] = {
                                            actual: strings.imagenes.url + strings.imagenes[tabla] + respuesta[aux][indice],
                                            pequena: strings.imagenes.url + strings.imagenes[tabla] + "P_" + respuesta[aux][indice],
                                            mediana: strings.imagenes.url + strings.imagenes[tabla] + "M_" + respuesta[aux][indice],
                                            grande: strings.imagenes.url + strings.imagenes[tabla] + "G_" + respuesta[aux][indice]
                                        }
                                    } else {
                                        respuesta[aux][indice] = {
                                            actual: strings.imagenes.url + strings.imagenes[tabla] + "default.png",
                                            pequena: strings.imagenes.url + strings.imagenes[tabla] + "default.png",
                                            mediana: strings.imagenes.url + strings.imagenes[tabla] + "default.png",
                                            grande: strings.imagenes.url + strings.imagenes[tabla] + "default.png"
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
        for (var aux in respuesta) {
            for (var indice in respuesta[aux]) {
                if ((typeof (modelos[tabla][indice]) !== "undefined") && (typeof (modelos[tabla][indice]["ref"]) !== "undefined") && (typeof (respuesta[aux]["_doc"]) !== "undefined") && (typeof (respuesta[aux]["_doc"][indice]) !== "undefined") && (typeof (respuesta[aux]["_doc"][indice]) === "object")) {
                    respuesta[aux]["_doc"][indice] = this.formatearPopulate(respuesta[aux]["_doc"][indice], modelos[tabla][indice]["ref"], null);
                }
            }
        }

        return callback(false, respuesta);
    },
    formatearPopulate: function (respuesta, tabla, rama) {
        var response = {};
        var indice = null;
        console.log(tabla);
        for (var indice in respuesta) {

            if ((typeof (modelos[tabla][indice]) !== "undefined") && (typeof (modelos[tabla][indice].dato) !== "undefined")) {
                if (typeof (respuesta) !== "undefined") {
                    if (typeof (respuesta[indice]) !== "undefined") {
                        //  console.log(modelos[tabla][indice].dato);
                        switch (modelos[tabla][indice].dato) {
                            case "imagen":

                                if (respuesta[indice] !== "") {
                                    response[indice] = {
                                        pequena: strings.imagenes.url + strings.imagenes[tabla] + "P_" + respuesta[indice],
                                        mediana: strings.imagenes.url + strings.imagenes[tabla] + "M_" + respuesta[indice],
                                        grande: strings.imagenes.url + strings.imagenes[tabla] + "G_" + respuesta[indice]
                                    }
                                } else {
                                    //  console.log("modificando -- " + indice + " -- " + tabla);
                                    //  console.log(respuesta);
                                    response[indice] = {
                                        pequena: strings.imagenes.url + strings.imagenes[tabla] + "default.png",
                                        mediana: strings.imagenes.url + strings.imagenes[tabla] + "default.png",
                                        grande: strings.imagenes.url + strings.imagenes[tabla] + "default.png"
                                    }
                                      console.log(response);
                                }
                                break;
                            case "arrayimagen":
                                //     console.log(respuesta["_doc"][indice]);
                                var arrayAux = [];
                                //   for (var indiimagen in respuesta["_doc"][indice]){
                                for (var indiimagen = 0; indiimagen < respuesta[indice].length; indiimagen++) {
                                    // console.log(indiimagen);
                                    arrayAux.push({
                                        pequena: strings.imagenes.url + strings.imagenes[tabla] + "P_" + respuesta[indice][indiimagen],
                                        mediana: strings.imagenes.url + strings.imagenes[tabla] + "M_" + respuesta[indice][indiimagen],
                                        grande: strings.imagenes.url + strings.imagenes[tabla] + "G_" + respuesta[indice][indiimagen]
                                    });
                                }
                                response[indice] = arrayAux;
                                break;
                            default:
                                if ((typeof (modelos[tabla][indice]["ref"]) !== "undefined") && (typeof (respuesta[indice]) === "object")) {
                                   // console.log("caiiiii");
                                    response[indice] = this.formatearPopulate(respuesta[indice], modelos[tabla][indice]["ref"], null);
                                }else{
                                    response[indice] = respuesta[indice];
                                }
                                
                                break;
                        }

                    }

                }
            }
        }
        //  console.log(respuesta);
        return   response;
    },
    crearPermalik: function (cadena) {
        return Math.floor((Math.random() * 9999) + 1) + "-" + cadena.replace(/Ã|À|Á|Ä|Â|È|É|Ë|Ê|Ì|Í|Ï|Î|Ò|Ó|Ö|Ô|Ù|Ú|Ü|Û|ã|à|á|ä|â|è|é|ë|ê|ì|í|ï|î|ò|ó|ö|ô|ù|ú|ü|û|Ñ|ñ|Ç|ç| /g, function myFunction(x) {
            var mal = "AEIOUÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç ";
            var bien = "aeiouaaaaaeeeeiiiioooouuuuaaaaaeeeeiiiioooouuuunncc-";
            return bien.charAt((mal.indexOf(x)));
        });
    },
    clearString: function (string) {
        return(string.replace(/\r?\n?/g, ""));
    },
    validarEmail: function (email) {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!expr.test(email)) {
            return false;
        } else {
            return true;
        }
    },
    //verifica si el array a esta en el array b
    intoArray: function (arraya, arrayb) {
        for (var i = 0; i < arraya.length && arrayb.indexOf(arraya[i]) >= 0; i++) {
        }
        return(i === arraya.length);
    },
    //verifica que el ojeto solo contenga los campos del array
    verificarBody: function (objeto, array, callback) {
        var i = 0;
        for (var aux in objeto) {
            i = i + 1;
            if (array.indexOf(aux) < 0) {
                return callback(false, "El campo " + aux + " no esta permitido.");
            }
        }
        //  console.log(array.length + "----" + i);
        if (array.length !== i) {
            return callback(false, "Debe pasar los campos " + array.toString());
        } else {
            return callback(true, "");
        }
    },
    // comvertimos un string en minuscula
    stringMinuscula: function (string) {
        return string.toLowerCase();
    },
    //se verifica la extension de un archivo
    verificarExtension: function (file)
    {
        //extensiones permitidas
        var allowedExtensions = ["jpg", "jpeg", "gif", "png", "rar", "pdf"];
        //extension del archivo
        var extension = file.split('.').pop();
        //hacemos la comprobación
        return this.in_array(extension, allowedExtensions) === true ? true : false;
    },
    contarPaginas: function (offset, limit, NumResult, callback) {
        numPage = Math.ceil(NumResult / limit);
        if (numPage < ((offset / limit) + 1))
        {
            if (numPage > 0) {
                return callback("El numero de Pagina ingresado " + ((offset / limit) + 1) + " es mayor al maximo permitido " + numPage + " para esta consulta", 0, 0);
            } else {
                return callback("No hay resultados para esta consulta", 0, 0);
            }
        } else {
            return callback(null, NumResult, numPage);
        }
    },
    eliminarElemento: function(array, item){
        var index = array.indexOf(item);
        if (index > -1) {
           array.splice(index, 1);
        }
        return array;
    }

};