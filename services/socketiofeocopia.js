var io = require('socket.io').listen(8000);
var conecciones = [];
var metodos = {
    nuevaConexion: function (id) {
        conecciones.push({"usuario": "anonimo", "id": id});
    },
    eliminarConexion: function (id) {
        for (var aux in conecciones){
            if (conecciones[aux].id===id){
                conecciones.splice( aux, 1 );
            }
        }
    },
    asociasUsuario: function(usuario, id){
        for (var aux in conecciones){
            if (conecciones[aux].id===id){
                conecciones[aux].usuario=usuario;
            }
        }
    },
    miUsuario: function(id){
       for (var aux in conecciones){
            if (conecciones[aux].id===id){
               return conecciones[aux];
            }
        } 
    },
    obtenerUsuario: function(usuario){
        var array = [];
       for (var aux in conecciones){
            if (conecciones[aux].usuario===usuario){
               array.push(conecciones[aux].id);
            }
        } 
        return array;
    }
}
io.on('connection', function (socket) {
    metodos.nuevaConexion(socket.id);
    io.emit('conectados',conecciones);
    console.log('a user connected: ' + socket.id);
    console.log(conecciones);
    socket.on('mensaje', function (data) {
        console.log(data);
            io.emit('respuesta',metodos.miUsuario(socket.id).usuario +": "+ data);
       // io.emit('respuesta',metodos.miUsuario(socket.id).usuario +": "+ data.mensaje);
    });
    socket.on('login', function (data) {
        console.log(data);
        metodos.asociasUsuario(data,socket.id);
    });
    socket.on('disconnect', function () {
        metodos.eliminarConexion(socket.id);
        console.log('user disconnected');
        io.emit('conectados',conecciones);
    });
    
});
