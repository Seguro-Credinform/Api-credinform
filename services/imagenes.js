//obtenemos el modelo UserModel con toda la funcionalidad
//creamos el ruteo de la aplicación
var fs = require('fs');//necesario para subir archivos
var gm = require('gm').subClass({imageMagick: true});
//var gm = require('gm');
module.exports = {
    subir: function (req, res, ubicacion, id, callback)
    {
        //ruta temporal, puede ser algo así C:\Users\User\AppData\Local\Temp\7056-12616ij.png
        var temporalPath = req.files.media.path;
        //ruta final donde alojaremos el archivo, le cambiamos el nombre para que 
        //sea estilo imagen-4365436.extension
        var nombreImagen = this.getFirstFileName(req.files.media.name, id);
        var finalPath = './archivos/' + ubicacion + '/' + nombreImagen[2];

        //si la extension no está permitida salimos con un mensaje
        if (this.checkExtension(req.files.media.name) === false)
        {
            var body = "El formato que intentas subir no está permitido :(";
            res.writeHead(200, {
                'Content-Length': body.length,
                'Content-Type': 'text/plain'
            });
            res.write(body);
            res.end();
        }

        //guardamos el archivo
        fs.exists(finalPath, function (exists)
        {
            //si existe
            if (exists)
            {
                nombreImagen = this.getNewFileName(req.files.media.name, id);
                //cambiamos el nombre del archivo
                finalPath = './archivos/' + ubicacion + '/' + nombreImagen[2];
            }
            //leemos y escribimos el nuevo archivo para guardarlo
            fs.rename(temporalPath, finalPath, function (error)
            {
                //si hay errores lanzamos una excepcion
                if (error)
                {
                    throw error;
                }
                console.log(' holaaaa ');
  fs.unlink(temporalPath, function ()
                                        {
                                            //si hay errores lanzamos una excepcion
                                            if (error)
                                            {
                                                throw error;
                                            }
                                           
                                            //return nombreImagen[3];
                                            // return nombreImagen;

                                        });
                                        console.log(' holaa ');
                gm(finalPath)
                        .resize(50, 50)
                        .autoOrient()
                        .write('./archivos/' + ubicacion + '/' + nombreImagen[0], function (err) {
                            if (!err){
                                console.log(' hooray! ');
                    }else{
                        console.log(err);
                    }
                            gm(finalPath)
                                    .resize(500, 500)
                                    .autoOrient()
                                    .write('./archivos/' + ubicacion + '/' + nombreImagen[1], function (err) {
                                        if (!err) {
                                            console.log(' hooray! ');
                                        }

                                
                                        return callback(nombreImagen);
                                    });
                        });
     
            });
        });

    },    subirComprobante: function (req, res, ubicacion, id, callback)
    {
        //ruta temporal, puede ser algo así C:\Users\User\AppData\Local\Temp\7056-12616ij.png
        var temporalPath = req.files.media.path;
        //ruta final donde alojaremos el archivo, le cambiamos el nombre para que 
        //sea estilo imagen-4365436.extension
        var nombreImagen = this.getFirstFileName(req.files.media.name, id);
        var finalPath = './archivos/' + ubicacion + '/' + nombreImagen[2];

        //si la extension no está permitida salimos con un mensaje
        if (this.checkExtensionDos(req.files.media.name) === false)
        {
            var body = "El formato que intentas subir no está permitido :(";
            res.writeHead(200, {
                'Content-Length': body.length,
                'Content-Type': 'text/plain'
            });
            res.write(body);
            res.end();
        }

        //guardamos el archivo
        fs.exists(finalPath, function (exists)
        {
            //si existe
            if (exists)
            {
                nombreImagen = this.getNewFileName(req.files.media.name, id);
                //cambiamos el nombre del archivo
                finalPath = './archivos/' + ubicacion + '/' + nombreImagen[2];
            }
            //leemos y escribimos el nuevo archivo para guardarlo
            fs.rename(temporalPath, finalPath, function (error)
            {
                //si hay errores lanzamos una excepcion
                if (error)
                {
                    throw error;
                }
  fs.unlink(temporalPath, function ()
                                        {
                                            //si hay errores lanzamos una excepcion
                                            if (error)
                                            {
                                                throw error;
                                            }
                                           
                                            //return nombreImagen[3];
                                            // return nombreImagen;

                                        });
                                        return callback(nombreImagen);
            });
        });

    },

    //obtiene el nuevo nombre del archivo si existe el anterior
    getNewFileName: function (file)
    {
        var f1 =  this.getIntRandom(10, 10000000) + "." + this.getExtension(file);
        var nombresito =  this.getIntRandom(10, 10000000) + this.getBetweenSeparators(f1);
        return ["P_" + nombresito + "_" + id + "." + this.getExtension(file), "M_" + nombresito + "_" + id + "." + this.getExtension(file), "G_" + nombresito + "_" + id + "." + this.getExtension(file), nombresito + "_" + id + "." + this.getExtension(file)]
    },
    //crea un nombre para la imagen a subir
    getFirstFileName: function (file, id)
    {
        var nombresito =  this.getIntRandom(10, 100000);
        return ["P_" + nombresito + "_" + id + "." + this.getExtension(file), "M_" + nombresito + "_" + id + "." + this.getExtension(file), "G_" + nombresito + "_" + id + "." + this.getExtension(file), nombresito + "_" + id + "." + this.getExtension(file)]

    },
    //obtenemos la extensión de la imagen
    getExtension: function (file)
    {
        return file.split('.').pop();
    },
    //obtenemos el nombre de la imagen
    getFileName: function (file)
    {
        return file.substr(0, file.lastIndexOf('.')) || file;
    },
    //obtenemos un número entero aleatorio para el nombre de la imagen
    getIntRandom: function (min, max)
    {
        return Math.floor((Math.random() * ((max + 1) - min)) + min);
    },
    //separamos entre el - y el .
    getBetweenSeparators: function (str)
    {
        return str.substring(str.lastIndexOf("-") + 1, str.lastIndexOf("."));
    },
    //comprobamos si está permitida la extensión del archivo
    checkExtension: function (file)
    {
        //extensiones permitidas
        var allowedExtensions = ["jpg", "jpeg", "gif", "png", "PNG"];
        //extension del archivo
        var extension = file.split('.').pop();
        //hacemos la comprobación
        return this.in_array(extension, allowedExtensions) === true ? true : false;
    },
    //comprobamos si está permitida la extensión del archivo
    checkExtensionDos: function (file)
    {
        //extensiones permitidas
        var allowedExtensions = ["jpg", "jpeg", "gif", "png", "PNG", "pdf"];
        //extension del archivo
        var extension = file.split('.').pop();
        //hacemos la comprobación
        return this.in_array(extension, allowedExtensions) === true ? true : false;
    }
    ,
    //funcion para comprobar valores en un array
    in_array: function (needle, haystack)
    {
        var key = '';
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
        return false;
    },
    comprimir: function (nombreImagen, ubicacion, callback)
    {
       gm('../archivos/' + ubicacion + '/'+nombreImagen[2])
                        .resize(50, 50)
                        .autoOrient()
                        .write('../archivos/' + ubicacion + '/' + nombreImagen[0], function (err) {
                            if (!err){
                                console.log(' hooray! ');
                    }else{
                        console.log(err);
                    }
                            gm('../archivos/' + ubicacion + '/'+nombreImagen[2])
                                    .resize(500, 500)
                                    .autoOrient()
                                    .write('../archivos/' + ubicacion + '/' + nombreImagen[1], function (err) {
                                        if (!err) {
                                            console.log(' hooray! ');
                                        }

                                
                                        return callback(nombreImagen);
                                    });
                        });
    }
}
