/* global models */
var conectorMongo = require('../services/conectorMongo');
var mongoBuilder = require('../services/mongoBuilder');
var tmp = require('../services/tmp');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var answers = require('../services/answers');
var httpHelpers = require('../services/httpHelpers');
var strings = require('../configs/strings');
var moment = require('moment');
var secret = '5cuguvZ4Az';
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

// Connect to MongoDB
mongoose.connect(strings.mongo.uri, strings.mongo.options);
mongoose.connection.on('error', function (err) {
    console.error('MongoDB connection error: ' + err);
    process.exit(-1);
});


try {
//elimina las sesiones venvidas de la base de datos
    mongoBuilder.eliminar("sesiones", {"ambiente": "web", "fecha": {$lt: moment().subtract('seconds', strings.sesion.tiempoExpiracion).toDate()}}, function (err) {
        if (err) {
            console.log('Esta sesion no existe');
        } else {
            console.log("Eliminada sesion");
        }
    });
} catch (err) {
    console.log(err);
    console.log("error in server");
}