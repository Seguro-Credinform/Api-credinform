var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
siniestros: {
        tipoSiniestro: {type: String, ref: 'tiposSiniestros'},
        fechaAccidente: {type: Date, required: true},
        fechaCreacion: {type: Date, default: Date.now},
        descripcion: {type: String, default: ""},
        usuario: {type: mongoose.Schema.Types.Mixed},
        fotos: {type: [String], default: []},
        direccion: {type: String, default: ""},
        latitud: {type: String, default: ""},
        longitud: {type: String, default: ""},
        status: {type: Number, default: 1}
    }
["_id","tipoSiniestro","fechaAccidente","fechaCreacion","descripcion","usuario","fotos","direccion","latitud","longitud","status"]
      */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("siniestros", req.query, [],["_id","tipoSiniestro","fechaAccidente","fechaCreacion","descripcion","usuario","fotos","direccion","latitud","longitud","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("siniestros", {"_id": req.params.id}, [],["_id","tipoSiniestro","fechaAccidente","fechaCreacion","descripcion","usuario","fotos","direccion","latitud","longitud","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("siniestros",  req.body, ["_id","tipoSiniestro","fechaAccidente","fechaCreacion","descripcion","usuario","fotos","direccion","latitud","longitud","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("siniestros", {"_id": req.params.id},  Object.assign(req.body), ["_id","tipoSiniestro","fechaAccidente","fechaCreacion","descripcion","usuario","fotos","direccion","latitud","longitud","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("siniestros", {"_id": req.params.id}, {"status": 0}, ["_id","tipoSiniestro","fechaAccidente","fechaCreacion","descripcion","usuario","fotos","direccion","latitud","longitud","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminarFisico: function (req, res) {
        try {
            mongoBuilder.eliminar("siniestros", {"_id": req.params.id}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    limpiar: function (req, res) {
        try {
            mongoBuilder.eliminar("siniestros", {}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
