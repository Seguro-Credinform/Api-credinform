var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
buzonSugerencias: {
        asunto: {type: String, default: ""},
        tipoSugerencia: {type: String, ref: 'tipoSugerencias'},
        mensaje: {type: String, default: ""},
        usuario: {type: mongoose.Schema.Types.Mixed},
        respuesta: {type: String, default: ""},
        status: {type: Number, default: 1}
    }
["_id","asunto","tipoSugerencia","direccion","mensaje","usuario","respuesta","status"]
      */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("sugerencias", req.query, [],["_id","asunto","tipoSugerencia","direccion","mensaje","usuario","respuesta","fechaCreacion","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("sugerencias", {"_id": req.params.id}, [],["_id","asunto","tipoSugerencia","direccion","mensaje","usuario","respuesta","fechaCreacion","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("sugerencias", req.body, ["_id","asunto","tipoSugerencia","direccion","mensaje","usuario","respuesta","fechaCreacion","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("sugerencias", {"_id": req.params.id},   req.body, ["_id","asunto","tipoSugerencia","direccion","mensaje","usuario","respuesta","fechaCreacion","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("sugerencias", {"_id": req.params.id}, {"status": 0}, ["_id","asunto","tipoSugerencia","direccion","mensaje","usuario","respuesta","fechaCreacion","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminarFisico: function (req, res) {
        try {
            mongoBuilder.eliminar("sugerencias", {"_id": req.params.id}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    limpiar: function (req, res) {
        try {
            mongoBuilder.eliminar("sugerencias", {}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
