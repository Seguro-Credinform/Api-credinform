var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
ubicaciones: {
        nombre: {type: String, default: ""},
        tipoUbicacion: {type: String, ref: 'tiposUbicaciones'},
        direccion: {type: String, default: ""},
        telefonos: {type: [String], default: []},
        correo: {type: [String], default: []},
        horario: {type: String, default: ""},
        latitud: {type: String, default: ""},
        longitud: {type: String, default: ""},
        status: {type: Number, default: 1}
    }
["_id","nombre","tipoUbicacion","direccion","telefonos","correo","horario","latitud","longitud","status"]
      */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("ubicaciones", req.query, [],["_id","nombre","tipoUbicacion","direccion","telefonos","correo","horario","latitud","longitud","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("ubicaciones", {"_id": req.params.id}, [],["_id","nombre","tipoUbicacion","direccion","telefonos","correo","horario","latitud","longitud","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("ubicaciones", req.body, ["_id","nombre","tipoUbicacion","direccion","telefonos","correo","horario","latitud","longitud","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("ubicaciones", {"_id": req.params.id},  req.body, ["_id","nombre","tipoUbicacion","direccion","telefonos","correo","horario","latitud","longitud","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("ubicaciones", {"_id": req.params.id}, {"status": 0}, ["_id","nombre","tipoUbicacion","direccion","telefonos","correo","horario","latitud","longitud","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminarFisico: function (req, res) {
        try {
            mongoBuilder.eliminar("ubicaciones", {"_id": req.params.id}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    limpiar: function (req, res) {
        try {
            mongoBuilder.eliminar("sugerencias", {}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
