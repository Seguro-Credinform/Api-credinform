var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
chats: {
        usuario: {type: mongoose.Schema.Types.Mixed},
        fecha: {type: Date, default: Date.now},
        tipoChat: {type: Number, default: 1}, //1:atencion al cliente
        status: {type: Number, default: 1}
    }
["_id","usuario","fecha","tipoChat","status"]
      */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("chats", req.query, [],["_id","usuario","fecha","tipoChat","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("chats", {"_id": req.params.id}, [],["_id","usuario","fecha","tipoChat","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("chats", req.body, ["_id","usuario","fecha","tipoChat","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("chats", {"_id": req.params.id}, req.body, ["_id","usuario","fecha","tipoChat","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("chats", {"_id": req.params.id}, {"status": 0}, ["_id","usuario","fecha","tipoChat","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminarFisico: function (req, res) {
        try {
            mongoBuilder.eliminar("chats", {"_id": req.params.id}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    limpiar: function (req, res) {
        try {
            mongoBuilder.eliminar("mensajes", {}, function (errr, resulta) {
                 mongoBuilder.eliminar("chats", {}, function (err, result) {
                answers.successCrear(result, res);
            });
            });
           
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
