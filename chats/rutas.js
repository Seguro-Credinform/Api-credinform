var rutas = require('express').Router();
var controlador = require('./controlador');
var mensaje = require('./controlador.mensajes');
var seguridad = require('../services/seguridad');

rutas.use(seguridad.autenticacionAdmin);
//rutas
rutas.get('/', controlador.listar);
rutas.get('/:id', controlador.obtener);
rutas.post('/', controlador.crear);

rutas.put('/:id', controlador.actualizar);
rutas.delete('/', controlador.limpiar);
rutas.delete('/:id', controlador.eliminar);
rutas.delete('/eliminar/:id', controlador.eliminarFisico);
rutas.get('/:idChat/mensajes/', mensaje.listar);
rutas.get('/:idChat/mensajes/:id', mensaje.obtener);
rutas.post('/:idChat/mensajes/', mensaje.crear);
rutas.put('/:idChat/mensajes/:id', mensaje.actualizar);




module.exports = rutas;