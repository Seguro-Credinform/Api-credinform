var express = require('express');
var app = express();
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var queryParser = require('express-query-int');
var answers = require('./services/answers');
var seguridad = require('./services/seguridad');




var  string = require('./configs/strings');
var  mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

// Connect to MongoDB
mongoose.connect(string.mongo.uri, string.mongo.options);
mongoose.connection.on('error', function(err) {
  console.error('MongoDB connection error: ' + err);
  process.exit(-1);
});











//var io = require('./services/socketio');
// rutas
/*
var categories = require('./categories/rutas');
var front = require('./front/rutas');
var questions = require('./questions/rutas');
*/

var multipart = require('connect-multiparty');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(queryParser());

app.use(multipart());
app.use(cookieParser());
/*app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});*/

app.use( function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
   // res.header('Access-Control-Allow-Headers', 'Content-Type');
   res.header("Access-Control-Allow-Headers", "Origin, x-access-llave, x-access-token, Content-Type, Accept");
   res.header("Access-control-expose-headers", "X-Total-Count, X-Number-Page");
    next();
});
app.use( function(req, res, next) {
if (req.method === 'OPTIONS') {
    res.status(200);
        res.end();
}
else {
    // Pass to next layer of middleware
    next();
}
});
//app.use(seguridad.verificarLlave);

app.use('/v1/usuarios', require('./usuarios/rutas'));
app.use('/v1/sesiones', require('./sesiones/rutas'));
app.use('/v1/documentos', require('./documentos/rutas'));
app.use('/v1/siniestros', require('./siniestros/rutas'));
app.use('/v1/ubicaciones', require('./ubicaciones/rutas'));
app.use('/v1/front', require('./front/rutas'));
app.use('/v1/preguntasFrecuentes', require('./preguntasFrecuentes/rutas'));
app.use('/v1/sugerencias', require('./sugerencias/rutas'));
app.use('/v1/chats', require('./chats/rutas'));
app.use('/v1/serviciosAdicionales', require('./serviciosAdicionales/rutas'));
app.use('/v1/serviciosAdicionalesSolicitudes', require('./serviciosAdicionalesSolicitudes/rutas'));



// catch 404 and forward to error handler
app.use(function (req, res, next) {
    answers.notFound(res);
});


// error handlers

// development error handler
// will print stacktrace
/*
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
*/
// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
module.exports = app;




