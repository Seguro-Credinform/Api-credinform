var rutas = require('express').Router();
var controlador = require('./controlador');
var seguridad = require('../services/seguridad');

rutas.use(seguridad.autenticacionAdmin);
//rutas
rutas.get('/', controlador.listar);
rutas.get('/:id', controlador.obtener);
rutas.post('/', controlador.crear);

rutas.put('/:id', controlador.actualizar);
rutas.delete('/', controlador.limpiar);
rutas.delete('/:id', controlador.eliminar);
rutas.delete('/eliminar/:id', controlador.eliminarFisico);




module.exports = rutas;