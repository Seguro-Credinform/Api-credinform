var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');
var strings = require('../configs/strings');
var crypto = require('crypto');

/*
name: usuarios: {
        nonmbre: {type: String, default: ""},
        tipo: {type: String, default: "web"},
        email:  {type: String, default: ""},
        password:  {type: String, default: ""},
        status: {type: Number, default: 1}
["nonmbre","tipo","email","status"]
    },
        information: {type: String, default: ""*/

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("usuarios", req.query, [],["_id","nombre","tipo","email","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("usuarios", {"_id": req.params.id}, [],["_id","nombre","tipo","email","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("usuarios", req.body, ["_id","nombre","tipo","email","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            if ((typeof (req.body.password) === "string") ) {
                req.body.password = crypto.createHmac('sha256', strings.sesion.hasClave)
                        .update(req.body.password)
                        .digest('hex');

            }
            mongoBuilder.actualizar("usuarios", {"_id": req.params.id}, req.body, ["_id","nombre","tipo","email","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminar: function (req, res) {
        try {
            mongoBuilder.actualizar("usuarios", {"_id": req.params.id}, {"status": 0}, ["_id","nombre","tipo","email","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminarFisico: function (req, res) {
        try {
            mongoBuilder.eliminar("usuarios", {"_id": req.params.id}, function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
