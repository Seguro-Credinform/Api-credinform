var rutas = require('express').Router();
var controlador = require('./controlador');
var seguridad = require('../services/seguridad');


rutas.post('/', seguridad.nuevoUsuario);
rutas.post('/login', seguridad.generarTokenAdmin);
//rutas.post('/front', controlador.crearFrontAdmin);

rutas.use(seguridad.autenticacionAdmin);

rutas.get('/', controlador.listar);
rutas.get('/:id', controlador.obtener);


rutas.put('/:id', controlador.actualizar);
rutas.delete('/:id', controlador.eliminar);
rutas.delete('/eliminar/:id', controlador.eliminarFisico);




module.exports = rutas;