var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
preguntasFrecuentes: {
        titulo: {type: String, default: ""},
        resumen: {type: String, default: ""},
        texto: {type: String, default: ""},
        imagen: {type: String, default: ""},
        status: {type: Number, default: 1}
    },
["titulo","resumen","texto","imagen","status"]
        */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("preguntasFrecuentes",Object.assign( req.query, {"status": 1}), [],["titulo","resumen","texto","imagen","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("preguntasFrecuentes", {"_id": req.params.id, "status": 1}, [],["titulo","resumen","texto","imagen","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
