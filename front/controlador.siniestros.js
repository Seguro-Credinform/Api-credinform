var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');
var helpers = require('./../services/helpers');
var tmp = require('../services/tmp');
var imagenesPost = require('../services/imagenesPost');
/*
 siniestros: {
 tipoSiniestro: {type: String, ref: 'tiposSiniestros'},
 fechaAccidente: {type: Date, required: true},
 fechaCreacion: {type: Date, default: Date.now},
 descripcion: {type: String, default: ""},
 usuario: {type: mongoose.Schema.Types.Mixed},
 fotos: {type: [String], default: []},
 direccion: {type: String, default: ""},
 latitud: {type: String, default: ""},
 longitud: {type: String, default: ""},
 status: {type: Number, default: 1}
 }
 ["_id","tipoSiniestro","fechaAccidente","fechaCreacion","descripcion","usuario","fotos","direccion","latitud","longitud","status"]
 */

/*
 [
 {
 "_id": "59b935b0a569a5184118ea25",
 "info": {
 "status": true,
 "Mensaje": "",
 "Usuario": {
 "Nombre": "JAVIER WILSON LOPEZ RIVEROS",
 "NumeroAsegurado":
 */


module.exports = {
    listar: function (req, res) {
        try {
            // answers.successCrear(tmp, res);
            console.log(tmp.usuarioActual.usuario);
            // mongoBuilder.obtener("siniestros", Object.assign( {"usuario": {"Usuario": { "NumeroAsegurado": tmp.usuarioActual.usuario.toString()  }}}, req.query), [], ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result, numResult, numPage) {
            mongoBuilder.obtener("siniestros", Object.assign({ "usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString() }, req.query), [], ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                    //   console.log( "Number of NumResult:", NumResult);
                    //   console.log( "Number of pagina:", numPage )
                    answers.success(result, numResult, numPage, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("siniestros", { "_id": req.params.id, "usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString() }, [], ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success(result, numResult, numPage, res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("siniestros", Object.assign({ usuario: tmp.usuarioActual.info }, req.body), ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizar: function (req, res) {
        try {
            mongoBuilder.actualizar("siniestros", { "_id": req.params.id }, Object.assign(req.body), ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    actualizarCoordenadas: function (req, res) {
        try {
            mongoBuilder.actualizar("siniestros", { "_id": req.params.id, usuario: tmp.usuarioActual.info }, Object.assign(req.body, { "status": 2 }), ["tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crearImagen: function (req, res) {
        try {
            imagenes.subir(req, res, "siniestros", req.params.id, function (imagen) {
                //queryBuilder.ejecutarConsultaActualizarCallback(modelos.categorias, {imagen: imagen[3]}, res, {idCategoria: req.params.id}, validadores.categorias.schema).then(function (result) {
                mongoBuilder.obtener("siniestros", { "_id": req.params.id, "usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString() }, [], ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result, numResult, numPage) {
                    if (err) {
                        answers.success(result, numResult, numPage, res);
                    } else {
                        var array = result[0]["_doc"]["fotosCopy"];
                        array.push(imagen[3]);
                        mongoBuilder.actualizar("siniestros", { "_id": req.params.id }, { fotos: array }, ["_id", "fotos"], function (err, result) {
                            //answers.successCrear({pequena: strings.imagenes.url + strings.imagenes.categorias + imagen[0], mediana: strings.imagenes.url + strings.imagenes.categorias + imagen[1], grande: strings.imagenes.url + strings.imagenes.categorias + imagen[2]}, res);
                            answers.successCrear(result, res);
                        });
                    }
                });

            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
        // answers.badRequest(imagen[3], res);

    },
    crearImagenPost: function (req, res) {
        try {
            imagenesPost.subir(req, res, "siniestros", req.params.id, function (imagen) {
                mongoBuilder.obtener("siniestros", { "_id": req.params.id, "usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString() }, [], ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result, numResult, numPage) {
                    if (err) {
                        answers.success(result, numResult, numPage, res);
                    } else {
                        var array = result[0]["_doc"]["fotosCopy"];
                        array.push(imagen[3]);
                        mongoBuilder.actualizar("siniestros", { "_id": req.params.id }, { fotos: array }, ["_id", "fotos"], function (err, result) {
                            //answers.successCrear({pequena: strings.imagenes.url + strings.imagenes.categorias + imagen[0], mediana: strings.imagenes.url + strings.imagenes.categorias + imagen[1], grande: strings.imagenes.url + strings.imagenes.categorias + imagen[2]}, res);
                            answers.successCrear(result, res);
                        });
                    }
                });
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    eliminarImagen: function (req, res) {
        try {
            mongoBuilder.obtener("siniestros", { "_id": req.params.id, "usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString() }, [], ["_id", "tipoSiniestro", "fechaAccidente", "fechaCreacion", "descripcion", "usuario", "fotos", "direccion", "latitud", "longitud", "status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success(result, numResult, numPage, res);
                } else {
                    var array = result[0]["_doc"]["fotosCopy"];
                    var arraynuevo = helpers.eliminarElemento(array, req.params.imagen);
                    mongoBuilder.actualizar("siniestros", { "_id": req.params.id }, { fotos: arraynuevo }, ["_id", "fotos"], function (err, result) {
                        //answers.successCrear({pequena: strings.imagenes.url + strings.imagenes.categorias + imagen[0], mediana: strings.imagenes.url + strings.imagenes.categorias + imagen[1], grande: strings.imagenes.url + strings.imagenes.categorias + imagen[2]}, res);
                        answers.successCrear(result, res);
                    });
                }
            });

        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
