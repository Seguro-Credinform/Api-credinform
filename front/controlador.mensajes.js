var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
mensajes: {
        chat: {type: String, ref: 'chats'},
        tipo: {type: Number, default: 1},//1:texto,2:audio,3:imagen
        mensaje: {type: String, default: ""},
        emisor: {type: Number, default: 1}, //1 cliente, 2 soporte
        fecha: {type: Date, default: Date.now}
    }
["_id","chat","tipo","direccion","mensaje","emisor","fecha"]
      */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("mensajes", Object.assign({"chat": req.params.idChat}, req.query), [],["_id","chat","tipo","direccion","mensaje","emisor","fecha"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("mensajes", {"chat": req.params.idChat,"_id": req.params.id}, [],["_id","chat","tipo","direccion","mensaje","emisor","fecha"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
