var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');

/*
tiposUbicaciones: {
        nombre: {type: String, default: ""},
        imagen: {type: String, default: ""},
        variableEnvio: {type: String, default: ""},
        status: {type: Number, default: 1}
    }
["_id","nombre","imagen","status"]
      */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("serviciosAdicionales", Object.assign( req.query, {"status": 1}), [],["_id","nombre","imagen","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("serviciosAdicionales", {"_id": req.params.id, "status": 1}, [],["_id","nombre","imagen","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
