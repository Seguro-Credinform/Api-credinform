var rutas = require('express').Router();
var siniestros = require('./controlador.siniestros');
var ubicaciones = require('./controlador.ubicaciones');
var sugerencias = require('./controlador.sugerencias');
var chats = require('./controlador.chats');
var mensajes = require('./controlador.mensajes');
var preguntasFrecuentes = require('./controlador.preguntasFrecuentes');
var seguridad = require('../services/seguridad');
var serviciosAdicionales = require('./controlador.serviciosAdicionales');
var serviciosAdicionalesSolicitudes = require('./controlador.serviciosAdicionalesSolicitudes');

//rutas.post('/front', controlador.crearFront);


//rutas
rutas.post('/login', seguridad.generarToken);
rutas.post('/renovar', seguridad.renovarSesion);

rutas.use(seguridad.autenticacion);
rutas.post('/desloguear', seguridad.eliminarSesion);
rutas.get('/siniestros', siniestros.listar);
rutas.get('/preguntasFrecuentes', preguntasFrecuentes.listar);
rutas.get('/ubicaciones', ubicaciones.listar);
rutas.get('/sugerencias', sugerencias.listar);
rutas.get('/serviciosAdicionales', serviciosAdicionales.listar);
rutas.get('/serviciosAdicionalesSolicitudes', serviciosAdicionalesSolicitudes.listar);
rutas.get('/chats', chats.listar);
rutas.get('/chats/:id', chats.obtener);
rutas.get('/serviciosAdicionalesSolicitudes/:id', serviciosAdicionalesSolicitudes.obtener);
rutas.get('/serviciosAdicionales/:id', serviciosAdicionales.obtener);
rutas.get('/siniestros/:id', siniestros.obtener);
rutas.get('/ubicaciones/:id', ubicaciones.obtener);
rutas.get('/sugerencias/:id', sugerencias.obtener);
rutas.get('/chats/:idChat/mensajes/', mensajes.listar);
rutas.get('/chats/:idChat/mensajes/:id', mensajes.obtener);
rutas.post('/siniestros', siniestros.crear);
rutas.post('/serviciosAdicionalesSolicitudes', serviciosAdicionalesSolicitudes.crear);
rutas.post('/chats', chats.crear);
rutas.post('/sugerencias', sugerencias.crear);
rutas.post('/siniestros/imagen/:id', siniestros.crearImagen);
rutas.post('/siniestros/imagen/post/:id',  siniestros.crearImagenPost);
rutas.put('/siniestros/:id', siniestros.actualizar);
rutas.put('/siniestros/:id/coordenas', siniestros.actualizarCoordenadas);
rutas.delete('/siniestros/:id/imagen/:imagen', siniestros.eliminarImagen);
//rutas.post('/reciclar', seguridad.reciclarSesiones);

//rutas.get('/users', users.obtener);
//rutas.put('/users', users.actualizar);
//rutas.get('/users/:id', controlador.listar);
//rutas.get('/:id', controlador.obtener);
//rutas.post('/', controlador.crear);





//rutas.put('/:id', controlador.actualizar);
//rutas.delete('/:id', controlador.eliminar);





module.exports = rutas;