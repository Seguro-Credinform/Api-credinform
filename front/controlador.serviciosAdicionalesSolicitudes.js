var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');
var tmp = require('../services/tmp');

/*
tiposUbicaciones: {
        nombre: {type: String, default: ""},
        imagen: {type: String, default: ""},
        variableEnvio: {type: String, default: ""},
        status: {type: Number, default: 1}
    }
["_id","nombre","imagen","status"]
      */
module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("serviciosAdicionalesSolicitudes", Object.assign( {"usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString()}, req.query), [],["_id","latitud","servicioAdicional","longitud","usuario","fechaCreacion","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("serviciosAdicionalesSolicitudes", {"_id": req.params.id,"usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString()}, [],["_id","latitud","servicioAdicional","longitud","usuario","fechaCreacion","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("serviciosAdicionalesSolicitudes", Object.assign({usuario: tmp.usuarioActual.info}, req.body), ["_id","latitud","longitud","servicioAdicional","usuario","fechaCreacion","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
