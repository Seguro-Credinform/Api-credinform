var mongoBuilder = require('./../services/mongoBuilder');
var answers = require('./../services/answers');
var tmp = require('../services/tmp');

/*
chats: {
        usuario: {type: mongoose.Schema.Types.Mixed},
        fecha: {type: Date, default: Date.now},
        tipoChat: {type: Number, default: 1}, //1:atencion al cliente
        status: {type: Number, default: 1}
    }
["_id","usuario","fecha","tipoChat","status"]
      */

module.exports = {
    listar: function (req, res) {
        try {
            mongoBuilder.obtener("chats", Object.assign( {"usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString()}, req.query), [],["_id","usuario","fecha","tipoChat","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.badRequest(err, res);
                } else {
                 //   console.log( "Number of NumResult:", NumResult);
                  //                       console.log( "Number of pagina:", numPage )
                    answers.success (result, numResult, numPage , res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    obtener: function (req, res) {
        try {
            mongoBuilder.obtener("chats", {"_id": req.params.id,"usuario.Usuario.NumeroAsegurado": tmp.usuarioActual.usuario.toString()}, [],["_id","usuario","fecha","tipoChat","status"], function (err, result, numResult, numPage) {
                if (err) {
                    answers.success (result, numResult, numPage , res);
                } else {
                    answers.successCrear(result, res);
                }
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    },
    crear: function (req, res) {
        try {
            mongoBuilder.crear("chats", Object.assign({usuario: tmp.usuarioActual.info}, req.body), ["_id","usuario","fecha","tipoChat","status"], function (err, result) {
                answers.successCrear(result, res);
            });
        } catch (err) {
            console.log(err);
            answers.backendError("error in server", res);
        }
    }
};
