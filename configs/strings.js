module.exports = {
    "imagenes":{
        //"url": "http://credinform-imagenes.tecomca.es",
        "url": "http://imagenes.admincrd.com",
        "tiposSugerencias":"/tiposSugerencias/",
        "preguntasFrecuentes":"/preguntasFrecuentes/",
        "serviciosAdicionales":"/serviciosAdicionales/",
        "tiposUbicaciones":"/tiposUbicaciones/",
        "siniestros":"/siniestros/",
        "iconos":"/iconos/",
        "tiposSiniestros":"/tiposSiniestros/"
    },
    "correos":{
        "recibidos" : "dbarrerapalacios@gmail.com"
    },
    "subApi": "http://190.129.70.58:8079",
    "sesion":{
        //"duracion":"60000 seconds",
        //"tiempoExpiracion":60000,//segundos,
        "duracion":"604800 seconds",
        "tiempoExpiracion":604800,//segundos,
        "hasEncriptacion":"shhhhh",
        "hasClave": "5cuguvZ4Az"
    },
    "mysql":{host: 'localhost', user: 'root', password: 'y4napana', database: 'credinform'},
    "mongo": {uri: 'mongodb://localhost:27017/credinform'}
   
}