var strings = require('../configs/strings');
var Sequelize = require('sequelize');
//database wide options
var sequelize = new Sequelize(strings.mysql.database, strings.mysql.user, strings.mysql.password, {
host: strings.mysql.host,
/*var sequelize = new Sequelize('tienda_laminados', 'root', '**t3c0mc4', {
   host: '159.203.19.72',*/
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }, define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true
    }
});

module.exports = sequelize;